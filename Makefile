SDL_CFLAGS	= -I/usr/include/SDL -D_REENTRANT
SDL_LDFLAGS	= -Llib -L/usr/lib -Wl,-rpath,/usr/lib -lSDL -lpthread -lSDL_image -lSDL_ttf -lSDL_gfx
GUICHAN_CFLAGS  = 
GUICHAN_LDFLAGS = -lguichan -guichan_sdl
CC		= g++
OPT		= -Wall -ansi -g
SRCDIR		= ./src
LIBDIR		= ./lib

CFLAGS		= $(OPT) -I$(LIBDIR) -I/usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3 $(SDL_CFLAGS) $(GUICHAN_CFLAGS)
LIBS		= $(OPT) $(SDL_LDFLAGS) $(GUICHAN_LDFLAGS)

OBJ		= main.o parse.o openspace.o spaceobjects.o spaceships.o projectils.o vector.o menu.o rank.o player.o
SRCS		= $(SRCDIR)/main.cpp $(SRCDIR)/parse.cpp $(SRCDIR)/openspace.cpp $(SRCDIR)/spaceobjects.cpp $(SRCDIR)/spaceships.cpp $(SRCDIR)/projectils.cpp $(SRCDIR)/vector.cpp $(SCRDIR)/menu.cpp  $(SCRDIR)/rank.cpp $(SRCDIR)/player.cpp
OUTFILE		= bin/oss

all: Makefile $(OBJ) 
	$(CC) $(LIBS) $(OBJ) -o $(OUTFILE)

gfxgen: $(SRCDIR)/gfxgen.cpp $(LIBDIR)/openspace.h Makefile
	$(CC) $(CFLAGS) $(LIBS) $(SRCDIR)/gfxgen.cpp -o gfxgen


%.o: $(SRCDIR)/%.cpp Makefile # $(LIBDIR)/%.h
	$(CC) $(CFLAGS) -c $<

depend:
	makedepend $(CFLAGS) $(SRCS) 2> /dev/null

clean: 
	rm -f $(OUTFILE) *.o



# DO NOT DELETE THIS LINE -- make depend depends on it.


src/main.o: lib/openspace.h /usr/include/SDL/SDL.h
src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
src/main.o: /usr/include/gnu/stubs.h
src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
src/main.o: /usr/include/SDL/SDL_image.h lib/parse.h lib/spaceobjects.h
src/main.o: /usr/include/SDL/SDL_ttf.h lib/vector.h lib/spaceships.h
src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h

./src/main.o: /usr/include/SDL/SDL_ttf.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/math.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/vector.o: /usr/include/bits/mathcalls.h

./src/main.o: /usr/include/SDL/SDL_ttf.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/math.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/vector.o: /usr/include/bits/mathcalls.h

./src/main.o: /usr/include/SDL/SDL_ttf.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/features.h
./src/spaceobjects.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/include/stdio.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/features.h
./src/spaceships.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/include/stdio.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/features.h
./src/projectils.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/include/stdio.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h

./src/main.o: /usr/include/SDL/SDL_ttf.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/features.h
./src/spaceobjects.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/include/stdio.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/features.h
./src/spaceships.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/include/stdio.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/features.h
./src/projectils.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/include/stdio.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/main.o: /usr/include/string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/main.o: /usr/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/main.o: /usr/include/stdlib.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/main.o: /usr/include/ctype.h /usr/include/endian.h
./src/main.o: /usr/include/bits/endian.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/main.o: /usr/include/time.h /usr/include/bits/time.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/main.o: /usr/include/SDL/SDL_image.h ./lib/parse.h ./lib/spaceobjects.h
./src/main.o: /usr/include/SDL/SDL_ttf.h ./lib/vector.h ./lib/spaceships.h
./src/main.o: ./lib/messages.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/main.o: /usr/include/SDL/SDL_rotozoom.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h /usr/include/SDL/SDL.h
./src/main.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/parse.o: /usr/include/ctype.h /usr/include/features.h
./src/parse.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/parse.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/parse.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/parse.o: /usr/include/bits/endian.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/parse.o: /usr/include/time.h /usr/include/bits/time.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/parse.o: /usr/include/string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/parse.o: /usr/include/stdio.h /usr/include/libio.h
./src/parse.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/parse.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/parse.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/parse.o: /usr/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/parse.o: /usr/include/stdlib.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/parse.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/parse.o: ./lib/parse.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/openspace.o: /usr/include/ctype.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/openspace.o: /usr/include/bits/endian.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/openspace.o: /usr/include/time.h /usr/include/bits/time.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/openspace.o: /usr/include/string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/openspace.o: /usr/include/stdio.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/openspace.o: /usr/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/openspace.o: /usr/include/stdlib.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/openspace.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/openspace.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/openspace.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/openspace.o: /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/openspace.o: ./lib/messages.h
./src/openspace.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceobjects.o: /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceobjects.o: /usr/include/string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceobjects.o: /usr/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceobjects.o: /usr/include/stdlib.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceobjects.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceobjects.o: /usr/include/bits/endian.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceobjects.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/sstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/fstream
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/locale
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.tcc
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cerrno
./src/spaceobjects.o: /usr/include/errno.h /usr/include/bits/errno.h
./src/spaceobjects.o: /usr/include/linux/errno.h /usr/include/asm/errno.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/clocale
./src/spaceobjects.o: /usr/include/locale.h /usr/include/bits/locale.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cmath
./src/spaceobjects.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/typeinfo
./src/spaceships.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/spaceships.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/spaceships.o: /usr/include/string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/spaceships.o: /usr/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/spaceships.o: /usr/include/stdlib.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/spaceships.o: /usr/include/ctype.h /usr/include/endian.h
./src/spaceships.o: /usr/include/bits/endian.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/spaceships.o: /usr/include/time.h /usr/include/bits/time.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/spaceships.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/projectils.o: ./lib/openspace.h /usr/include/SDL/SDL.h
./src/projectils.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/list
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/projectils.o: /usr/include/string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/projectils.o: /usr/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/projectils.o: /usr/include/stdlib.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/projectils.o: /usr/include/ctype.h /usr/include/endian.h
./src/projectils.o: /usr/include/bits/endian.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/projectils.o: /usr/include/time.h /usr/include/bits/time.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_list.h
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h ./lib/projectils.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/projectils.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ostream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ios
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/iosfwd
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cctype
./src/vector.o: /usr/include/ctype.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stddef.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/endian.h
./src/vector.o: /usr/include/bits/endian.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stringfwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/fpos.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwchar
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstddef
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/ctime
./src/vector.o: /usr/include/time.h /usr/include/bits/time.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/functexcept.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception_defines.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/exception
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/char_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstring
./src/vector.o: /usr/include/string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdio
./src/vector.o: /usr/include/stdio.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/localefwd.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/ios_base.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_classes.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/string
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/memory
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_algobase.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/climits
./src/vector.o: /usr/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/limits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cstdlib
./src/vector.o: /usr/include/stdlib.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/new
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_pair.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/type_traits.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_types.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator_base_funcs.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/concept_check.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_alloc.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_threads.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_construct.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_uninitialized.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_raw_storage_iter.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/stl_function.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_string.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/streambuf
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/basic_ios.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/streambuf_iterator.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/locale_facets.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/cwctype
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/bits/codecvt.h
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/istream
./src/vector.o: /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.4/include/g++-v3/limits
./src/vector.o: /usr/include/math.h /usr/include/bits/huge_val.h
./src/vector.o: /usr/include/bits/mathdef.h /usr/include/bits/mathcalls.h
./src/vector.o: ./lib/vector.h /usr/include/SDL/SDL.h
./src/vector.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/vector.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/vector.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/vector.o: /usr/include/SDL/SDL_rwops.h /usr/include/SDL/SDL_timer.h
./src/vector.o: /usr/include/SDL/SDL_audio.h /usr/include/SDL/SDL_byteorder.h
./src/vector.o: /usr/include/SDL/SDL_cdrom.h /usr/include/SDL/SDL_joystick.h
./src/vector.o: /usr/include/SDL/SDL_events.h /usr/include/SDL/SDL_active.h
./src/vector.o: /usr/include/SDL/SDL_keyboard.h /usr/include/SDL/SDL_keysym.h
./src/vector.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/vector.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/vector.o: /usr/include/SDL/SDL_version.h

./src/main.o: ./lib/openspace.h /usr/include/guichan.hpp
./src/main.o: /usr/include/guichan/actionlistener.hpp
./src/main.o: /usr/include/guichan/platform.hpp
./src/main.o: /usr/include/guichan/graphics.hpp
./src/main.o: /usr/include/guichan/cliprectangle.hpp
./src/main.o: /usr/include/guichan/rectangle.hpp
./src/main.o: /usr/include/guichan/color.hpp /usr/include/guichan/image.hpp
./src/main.o: /usr/include/guichan/imageloader.hpp
./src/main.o: /usr/include/guichan/exception.hpp
./src/main.o: /usr/include/guichan/focushandler.hpp
./src/main.o: /usr/include/guichan/widget.hpp
./src/main.o: /usr/include/guichan/defaultfont.hpp
./src/main.o: /usr/include/guichan/font.hpp /usr/include/guichan/keyinput.hpp
./src/main.o: /usr/include/guichan/key.hpp
./src/main.o: /usr/include/guichan/keylistener.hpp
./src/main.o: /usr/include/guichan/mouseinput.hpp
./src/main.o: /usr/include/guichan/mouselistener.hpp
./src/main.o: /usr/include/guichan/gui.hpp /usr/include/guichan/input.hpp
./src/main.o: /usr/include/guichan/imagefont.hpp
./src/main.o: /usr/include/guichan/listmodel.hpp
./src/main.o: /usr/include/guichan/widgets/button.hpp
./src/main.o: /usr/include/guichan/widgets/checkbox.hpp
./src/main.o: /usr/include/guichan/widgets/container.hpp
./src/main.o: /usr/include/guichan/basiccontainer.hpp
./src/main.o: /usr/include/guichan/widgets/dropdown.hpp
./src/main.o: /usr/include/guichan/widgets/listbox.hpp
./src/main.o: /usr/include/guichan/widgets/scrollarea.hpp
./src/main.o: /usr/include/guichan/widgets/icon.hpp
./src/main.o: /usr/include/guichan/widgets/label.hpp
./src/main.o: /usr/include/guichan/widgets/slider.hpp
./src/main.o: /usr/include/guichan/widgets/radiobutton.hpp
./src/main.o: /usr/include/guichan/widgets/textbox.hpp
./src/main.o: /usr/include/guichan/widgets/textfield.hpp
./src/main.o: /usr/include/guichan/widgets/window.hpp
./src/main.o: /usr/include/guichan/sdl.hpp
./src/main.o: /usr/include/guichan/sdl/sdlgraphics.hpp /usr/include/SDL/SDL.h
./src/main.o: /usr/include/SDL/SDL_main.h /usr/include/SDL/SDL_types.h
./src/main.o: /usr/include/SDL/SDL_getenv.h /usr/include/SDL/SDL_error.h
./src/main.o: /usr/include/SDL/begin_code.h /usr/include/SDL/close_code.h
./src/main.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/main.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/main.o: /usr/include/gnu/stubs.h
./src/main.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stddef.h
./src/main.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/main.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/main.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/main.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/main.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stdarg.h
./src/main.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/main.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/main.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/main.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/main.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/main.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/main.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/main.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/main.o: /usr/include/guichan/sdl/sdlimageloader.hpp
./src/main.o: /usr/include/guichan/sdl/sdlinput.hpp
./src/main.o: /usr/include/guichan/platform.hpp /usr/include/SDL/SDL_image.h
./src/main.o: ./lib/parse.h ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/main.o: ./lib/vector.h ./lib/spaceships.h /usr/include/math.h
./src/main.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/main.o: /usr/include/bits/mathcalls.h ./lib/messages.h
./src/main.o: /usr/include/SDL/SDL_rotozoom.h
./src/parse.o: ./lib/parse.h
./src/openspace.o: ./lib/openspace.h /usr/include/guichan.hpp
./src/openspace.o: /usr/include/guichan/actionlistener.hpp
./src/openspace.o: /usr/include/guichan/platform.hpp
./src/openspace.o: /usr/include/guichan/graphics.hpp
./src/openspace.o: /usr/include/guichan/cliprectangle.hpp
./src/openspace.o: /usr/include/guichan/rectangle.hpp
./src/openspace.o: /usr/include/guichan/color.hpp
./src/openspace.o: /usr/include/guichan/image.hpp
./src/openspace.o: /usr/include/guichan/imageloader.hpp
./src/openspace.o: /usr/include/guichan/exception.hpp
./src/openspace.o: /usr/include/guichan/focushandler.hpp
./src/openspace.o: /usr/include/guichan/widget.hpp
./src/openspace.o: /usr/include/guichan/defaultfont.hpp
./src/openspace.o: /usr/include/guichan/font.hpp
./src/openspace.o: /usr/include/guichan/keyinput.hpp
./src/openspace.o: /usr/include/guichan/key.hpp
./src/openspace.o: /usr/include/guichan/keylistener.hpp
./src/openspace.o: /usr/include/guichan/mouseinput.hpp
./src/openspace.o: /usr/include/guichan/mouselistener.hpp
./src/openspace.o: /usr/include/guichan/gui.hpp
./src/openspace.o: /usr/include/guichan/input.hpp
./src/openspace.o: /usr/include/guichan/imagefont.hpp
./src/openspace.o: /usr/include/guichan/listmodel.hpp
./src/openspace.o: /usr/include/guichan/widgets/button.hpp
./src/openspace.o: /usr/include/guichan/widgets/checkbox.hpp
./src/openspace.o: /usr/include/guichan/widgets/container.hpp
./src/openspace.o: /usr/include/guichan/basiccontainer.hpp
./src/openspace.o: /usr/include/guichan/widgets/dropdown.hpp
./src/openspace.o: /usr/include/guichan/widgets/listbox.hpp
./src/openspace.o: /usr/include/guichan/widgets/scrollarea.hpp
./src/openspace.o: /usr/include/guichan/widgets/icon.hpp
./src/openspace.o: /usr/include/guichan/widgets/label.hpp
./src/openspace.o: /usr/include/guichan/widgets/slider.hpp
./src/openspace.o: /usr/include/guichan/widgets/radiobutton.hpp
./src/openspace.o: /usr/include/guichan/widgets/textbox.hpp
./src/openspace.o: /usr/include/guichan/widgets/textfield.hpp
./src/openspace.o: /usr/include/guichan/widgets/window.hpp
./src/openspace.o: /usr/include/guichan/sdl.hpp
./src/openspace.o: /usr/include/guichan/sdl/sdlgraphics.hpp
./src/openspace.o: /usr/include/SDL/SDL.h /usr/include/SDL/SDL_main.h
./src/openspace.o: /usr/include/SDL/SDL_types.h /usr/include/SDL/SDL_getenv.h
./src/openspace.o: /usr/include/SDL/SDL_error.h /usr/include/SDL/begin_code.h
./src/openspace.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/openspace.o: /usr/include/stdio.h /usr/include/features.h
./src/openspace.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/openspace.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stddef.h
./src/openspace.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/openspace.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/openspace.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/openspace.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/openspace.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stdarg.h
./src/openspace.o: /usr/include/bits/stdio_lim.h
./src/openspace.o: /usr/include/bits/sys_errlist.h
./src/openspace.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/openspace.o: /usr/include/SDL/SDL_byteorder.h
./src/openspace.o: /usr/include/SDL/SDL_cdrom.h
./src/openspace.o: /usr/include/SDL/SDL_joystick.h
./src/openspace.o: /usr/include/SDL/SDL_events.h
./src/openspace.o: /usr/include/SDL/SDL_active.h
./src/openspace.o: /usr/include/SDL/SDL_keyboard.h
./src/openspace.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/openspace.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/openspace.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
./src/openspace.o: /usr/include/guichan/sdl/sdlimageloader.hpp
./src/openspace.o: /usr/include/guichan/sdl/sdlinput.hpp
./src/openspace.o: /usr/include/guichan/platform.hpp
./src/openspace.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/openspace.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/openspace.o: ./lib/vector.h ./lib/spaceships.h /usr/include/math.h
./src/openspace.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/openspace.o: /usr/include/bits/mathcalls.h ./lib/projectils.h
./src/openspace.o: ./lib/messages.h
./src/spaceobjects.o: ./lib/openspace.h /usr/include/guichan.hpp
./src/spaceobjects.o: /usr/include/guichan/actionlistener.hpp
./src/spaceobjects.o: /usr/include/guichan/platform.hpp
./src/spaceobjects.o: /usr/include/guichan/graphics.hpp
./src/spaceobjects.o: /usr/include/guichan/cliprectangle.hpp
./src/spaceobjects.o: /usr/include/guichan/rectangle.hpp
./src/spaceobjects.o: /usr/include/guichan/color.hpp
./src/spaceobjects.o: /usr/include/guichan/image.hpp
./src/spaceobjects.o: /usr/include/guichan/imageloader.hpp
./src/spaceobjects.o: /usr/include/guichan/exception.hpp
./src/spaceobjects.o: /usr/include/guichan/focushandler.hpp
./src/spaceobjects.o: /usr/include/guichan/widget.hpp
./src/spaceobjects.o: /usr/include/guichan/defaultfont.hpp
./src/spaceobjects.o: /usr/include/guichan/font.hpp
./src/spaceobjects.o: /usr/include/guichan/keyinput.hpp
./src/spaceobjects.o: /usr/include/guichan/key.hpp
./src/spaceobjects.o: /usr/include/guichan/keylistener.hpp
./src/spaceobjects.o: /usr/include/guichan/mouseinput.hpp
./src/spaceobjects.o: /usr/include/guichan/mouselistener.hpp
./src/spaceobjects.o: /usr/include/guichan/gui.hpp
./src/spaceobjects.o: /usr/include/guichan/input.hpp
./src/spaceobjects.o: /usr/include/guichan/imagefont.hpp
./src/spaceobjects.o: /usr/include/guichan/listmodel.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/button.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/checkbox.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/container.hpp
./src/spaceobjects.o: /usr/include/guichan/basiccontainer.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/dropdown.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/listbox.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/scrollarea.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/icon.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/label.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/slider.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/radiobutton.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/textbox.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/textfield.hpp
./src/spaceobjects.o: /usr/include/guichan/widgets/window.hpp
./src/spaceobjects.o: /usr/include/guichan/sdl.hpp
./src/spaceobjects.o: /usr/include/guichan/sdl/sdlgraphics.hpp
./src/spaceobjects.o: /usr/include/SDL/SDL.h /usr/include/SDL/SDL_main.h
./src/spaceobjects.o: /usr/include/SDL/SDL_types.h
./src/spaceobjects.o: /usr/include/SDL/SDL_getenv.h
./src/spaceobjects.o: /usr/include/SDL/SDL_error.h
./src/spaceobjects.o: /usr/include/SDL/begin_code.h
./src/spaceobjects.o: /usr/include/SDL/close_code.h
./src/spaceobjects.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceobjects.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceobjects.o: /usr/include/gnu/stubs.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stddef.h
./src/spaceobjects.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceobjects.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceobjects.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceobjects.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceobjects.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stdarg.h
./src/spaceobjects.o: /usr/include/bits/stdio_lim.h
./src/spaceobjects.o: /usr/include/bits/sys_errlist.h
./src/spaceobjects.o: /usr/include/SDL/SDL_timer.h
./src/spaceobjects.o: /usr/include/SDL/SDL_audio.h
./src/spaceobjects.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceobjects.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceobjects.o: /usr/include/SDL/SDL_joystick.h
./src/spaceobjects.o: /usr/include/SDL/SDL_events.h
./src/spaceobjects.o: /usr/include/SDL/SDL_active.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceobjects.o: /usr/include/SDL/SDL_keysym.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mouse.h
./src/spaceobjects.o: /usr/include/SDL/SDL_video.h
./src/spaceobjects.o: /usr/include/SDL/SDL_mutex.h
./src/spaceobjects.o: /usr/include/SDL/SDL_quit.h
./src/spaceobjects.o: /usr/include/SDL/SDL_version.h
./src/spaceobjects.o: /usr/include/guichan/sdl/sdlimageloader.hpp
./src/spaceobjects.o: /usr/include/guichan/sdl/sdlinput.hpp
./src/spaceobjects.o: /usr/include/guichan/platform.hpp
./src/spaceobjects.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceobjects.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceobjects.o: ./lib/vector.h ./lib/spaceships.h /usr/include/math.h
./src/spaceobjects.o: /usr/include/bits/huge_val.h
./src/spaceobjects.o: /usr/include/bits/mathdef.h
./src/spaceobjects.o: /usr/include/bits/mathcalls.h
./src/spaceships.o: ./lib/openspace.h /usr/include/guichan.hpp
./src/spaceships.o: /usr/include/guichan/actionlistener.hpp
./src/spaceships.o: /usr/include/guichan/platform.hpp
./src/spaceships.o: /usr/include/guichan/graphics.hpp
./src/spaceships.o: /usr/include/guichan/cliprectangle.hpp
./src/spaceships.o: /usr/include/guichan/rectangle.hpp
./src/spaceships.o: /usr/include/guichan/color.hpp
./src/spaceships.o: /usr/include/guichan/image.hpp
./src/spaceships.o: /usr/include/guichan/imageloader.hpp
./src/spaceships.o: /usr/include/guichan/exception.hpp
./src/spaceships.o: /usr/include/guichan/focushandler.hpp
./src/spaceships.o: /usr/include/guichan/widget.hpp
./src/spaceships.o: /usr/include/guichan/defaultfont.hpp
./src/spaceships.o: /usr/include/guichan/font.hpp
./src/spaceships.o: /usr/include/guichan/keyinput.hpp
./src/spaceships.o: /usr/include/guichan/key.hpp
./src/spaceships.o: /usr/include/guichan/keylistener.hpp
./src/spaceships.o: /usr/include/guichan/mouseinput.hpp
./src/spaceships.o: /usr/include/guichan/mouselistener.hpp
./src/spaceships.o: /usr/include/guichan/gui.hpp
./src/spaceships.o: /usr/include/guichan/input.hpp
./src/spaceships.o: /usr/include/guichan/imagefont.hpp
./src/spaceships.o: /usr/include/guichan/listmodel.hpp
./src/spaceships.o: /usr/include/guichan/widgets/button.hpp
./src/spaceships.o: /usr/include/guichan/widgets/checkbox.hpp
./src/spaceships.o: /usr/include/guichan/widgets/container.hpp
./src/spaceships.o: /usr/include/guichan/basiccontainer.hpp
./src/spaceships.o: /usr/include/guichan/widgets/dropdown.hpp
./src/spaceships.o: /usr/include/guichan/widgets/listbox.hpp
./src/spaceships.o: /usr/include/guichan/widgets/scrollarea.hpp
./src/spaceships.o: /usr/include/guichan/widgets/icon.hpp
./src/spaceships.o: /usr/include/guichan/widgets/label.hpp
./src/spaceships.o: /usr/include/guichan/widgets/slider.hpp
./src/spaceships.o: /usr/include/guichan/widgets/radiobutton.hpp
./src/spaceships.o: /usr/include/guichan/widgets/textbox.hpp
./src/spaceships.o: /usr/include/guichan/widgets/textfield.hpp
./src/spaceships.o: /usr/include/guichan/widgets/window.hpp
./src/spaceships.o: /usr/include/guichan/sdl.hpp
./src/spaceships.o: /usr/include/guichan/sdl/sdlgraphics.hpp
./src/spaceships.o: /usr/include/SDL/SDL.h /usr/include/SDL/SDL_main.h
./src/spaceships.o: /usr/include/SDL/SDL_types.h
./src/spaceships.o: /usr/include/SDL/SDL_getenv.h
./src/spaceships.o: /usr/include/SDL/SDL_error.h
./src/spaceships.o: /usr/include/SDL/begin_code.h
./src/spaceships.o: /usr/include/SDL/close_code.h
./src/spaceships.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/spaceships.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/spaceships.o: /usr/include/gnu/stubs.h
./src/spaceships.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stddef.h
./src/spaceships.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/spaceships.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/spaceships.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/spaceships.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/spaceships.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stdarg.h
./src/spaceships.o: /usr/include/bits/stdio_lim.h
./src/spaceships.o: /usr/include/bits/sys_errlist.h
./src/spaceships.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/spaceships.o: /usr/include/SDL/SDL_byteorder.h
./src/spaceships.o: /usr/include/SDL/SDL_cdrom.h
./src/spaceships.o: /usr/include/SDL/SDL_joystick.h
./src/spaceships.o: /usr/include/SDL/SDL_events.h
./src/spaceships.o: /usr/include/SDL/SDL_active.h
./src/spaceships.o: /usr/include/SDL/SDL_keyboard.h
./src/spaceships.o: /usr/include/SDL/SDL_keysym.h
./src/spaceships.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/spaceships.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/spaceships.o: /usr/include/SDL/SDL_version.h
./src/spaceships.o: /usr/include/guichan/sdl/sdlimageloader.hpp
./src/spaceships.o: /usr/include/guichan/sdl/sdlinput.hpp
./src/spaceships.o: /usr/include/guichan/platform.hpp
./src/spaceships.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/spaceships.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/spaceships.o: ./lib/vector.h ./lib/spaceships.h /usr/include/math.h
./src/spaceships.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/spaceships.o: /usr/include/bits/mathcalls.h ./lib/projectils.h
./src/projectils.o: ./lib/openspace.h /usr/include/guichan.hpp
./src/projectils.o: /usr/include/guichan/actionlistener.hpp
./src/projectils.o: /usr/include/guichan/platform.hpp
./src/projectils.o: /usr/include/guichan/graphics.hpp
./src/projectils.o: /usr/include/guichan/cliprectangle.hpp
./src/projectils.o: /usr/include/guichan/rectangle.hpp
./src/projectils.o: /usr/include/guichan/color.hpp
./src/projectils.o: /usr/include/guichan/image.hpp
./src/projectils.o: /usr/include/guichan/imageloader.hpp
./src/projectils.o: /usr/include/guichan/exception.hpp
./src/projectils.o: /usr/include/guichan/focushandler.hpp
./src/projectils.o: /usr/include/guichan/widget.hpp
./src/projectils.o: /usr/include/guichan/defaultfont.hpp
./src/projectils.o: /usr/include/guichan/font.hpp
./src/projectils.o: /usr/include/guichan/keyinput.hpp
./src/projectils.o: /usr/include/guichan/key.hpp
./src/projectils.o: /usr/include/guichan/keylistener.hpp
./src/projectils.o: /usr/include/guichan/mouseinput.hpp
./src/projectils.o: /usr/include/guichan/mouselistener.hpp
./src/projectils.o: /usr/include/guichan/gui.hpp
./src/projectils.o: /usr/include/guichan/input.hpp
./src/projectils.o: /usr/include/guichan/imagefont.hpp
./src/projectils.o: /usr/include/guichan/listmodel.hpp
./src/projectils.o: /usr/include/guichan/widgets/button.hpp
./src/projectils.o: /usr/include/guichan/widgets/checkbox.hpp
./src/projectils.o: /usr/include/guichan/widgets/container.hpp
./src/projectils.o: /usr/include/guichan/basiccontainer.hpp
./src/projectils.o: /usr/include/guichan/widgets/dropdown.hpp
./src/projectils.o: /usr/include/guichan/widgets/listbox.hpp
./src/projectils.o: /usr/include/guichan/widgets/scrollarea.hpp
./src/projectils.o: /usr/include/guichan/widgets/icon.hpp
./src/projectils.o: /usr/include/guichan/widgets/label.hpp
./src/projectils.o: /usr/include/guichan/widgets/slider.hpp
./src/projectils.o: /usr/include/guichan/widgets/radiobutton.hpp
./src/projectils.o: /usr/include/guichan/widgets/textbox.hpp
./src/projectils.o: /usr/include/guichan/widgets/textfield.hpp
./src/projectils.o: /usr/include/guichan/widgets/window.hpp
./src/projectils.o: /usr/include/guichan/sdl.hpp
./src/projectils.o: /usr/include/guichan/sdl/sdlgraphics.hpp
./src/projectils.o: /usr/include/SDL/SDL.h /usr/include/SDL/SDL_main.h
./src/projectils.o: /usr/include/SDL/SDL_types.h
./src/projectils.o: /usr/include/SDL/SDL_getenv.h
./src/projectils.o: /usr/include/SDL/SDL_error.h
./src/projectils.o: /usr/include/SDL/begin_code.h
./src/projectils.o: /usr/include/SDL/close_code.h
./src/projectils.o: /usr/include/SDL/SDL_rwops.h /usr/include/stdio.h
./src/projectils.o: /usr/include/features.h /usr/include/sys/cdefs.h
./src/projectils.o: /usr/include/gnu/stubs.h
./src/projectils.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stddef.h
./src/projectils.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/projectils.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/projectils.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/projectils.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/projectils.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stdarg.h
./src/projectils.o: /usr/include/bits/stdio_lim.h
./src/projectils.o: /usr/include/bits/sys_errlist.h
./src/projectils.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/projectils.o: /usr/include/SDL/SDL_byteorder.h
./src/projectils.o: /usr/include/SDL/SDL_cdrom.h
./src/projectils.o: /usr/include/SDL/SDL_joystick.h
./src/projectils.o: /usr/include/SDL/SDL_events.h
./src/projectils.o: /usr/include/SDL/SDL_active.h
./src/projectils.o: /usr/include/SDL/SDL_keyboard.h
./src/projectils.o: /usr/include/SDL/SDL_keysym.h
./src/projectils.o: /usr/include/SDL/SDL_mouse.h /usr/include/SDL/SDL_video.h
./src/projectils.o: /usr/include/SDL/SDL_mutex.h /usr/include/SDL/SDL_quit.h
./src/projectils.o: /usr/include/SDL/SDL_version.h
./src/projectils.o: /usr/include/guichan/sdl/sdlimageloader.hpp
./src/projectils.o: /usr/include/guichan/sdl/sdlinput.hpp
./src/projectils.o: /usr/include/guichan/platform.hpp
./src/projectils.o: /usr/include/SDL/SDL_image.h ./lib/parse.h
./src/projectils.o: ./lib/spaceobjects.h /usr/include/SDL/SDL_ttf.h
./src/projectils.o: ./lib/vector.h ./lib/spaceships.h /usr/include/math.h
./src/projectils.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/projectils.o: /usr/include/bits/mathcalls.h ./lib/projectils.h
./src/vector.o: /usr/include/math.h /usr/include/features.h
./src/vector.o: /usr/include/sys/cdefs.h /usr/include/gnu/stubs.h
./src/vector.o: /usr/include/bits/huge_val.h /usr/include/bits/mathdef.h
./src/vector.o: /usr/include/bits/mathcalls.h ./lib/vector.h
./src/vector.o: /usr/include/SDL/SDL.h /usr/include/SDL/SDL_main.h
./src/vector.o: /usr/include/SDL/SDL_types.h /usr/include/SDL/SDL_getenv.h
./src/vector.o: /usr/include/SDL/SDL_error.h /usr/include/SDL/begin_code.h
./src/vector.o: /usr/include/SDL/close_code.h /usr/include/SDL/SDL_rwops.h
./src/vector.o: /usr/include/stdio.h
./src/vector.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stddef.h
./src/vector.o: /usr/include/bits/types.h /usr/include/bits/wordsize.h
./src/vector.o: /usr/include/bits/typesizes.h /usr/include/libio.h
./src/vector.o: /usr/include/_G_config.h /usr/include/wchar.h
./src/vector.o: /usr/include/bits/wchar.h /usr/include/gconv.h
./src/vector.o: /usr/lib/gcc-lib/i586-pc-linux-gnu/3.3.5-20050130/include/stdarg.h
./src/vector.o: /usr/include/bits/stdio_lim.h /usr/include/bits/sys_errlist.h
./src/vector.o: /usr/include/SDL/SDL_timer.h /usr/include/SDL/SDL_audio.h
./src/vector.o: /usr/include/SDL/SDL_byteorder.h /usr/include/SDL/SDL_cdrom.h
./src/vector.o: /usr/include/SDL/SDL_joystick.h /usr/include/SDL/SDL_events.h
./src/vector.o: /usr/include/SDL/SDL_active.h /usr/include/SDL/SDL_keyboard.h
./src/vector.o: /usr/include/SDL/SDL_keysym.h /usr/include/SDL/SDL_mouse.h
./src/vector.o: /usr/include/SDL/SDL_video.h /usr/include/SDL/SDL_mutex.h
./src/vector.o: /usr/include/SDL/SDL_quit.h /usr/include/SDL/SDL_version.h
