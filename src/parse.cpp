#include <iostream>
#include "parse.h"


// dafaultwerte f�r level und lifes, fullscreen und maus-modus (no mouse)
int lv=0, li=3;
bool fs=false, nm=false;


int parse(int argc, char **argv)
{
  int returnvalue = 0;
  for(int i = 1;i < argc; i++)
    {
      if ((argv[i])[0] == '-')
	// hier wurde ein Parameter als solcher erkannt
	{
	  if (strcmp(argv[i],"-lv") == 0){      	
	    if (i+1 < argc)
	      lv = atoi(argv[i+1]);
	    i++;
	  }
	  else if (strcmp(argv[i], "-li") == 0){
	    if (i+1 < argc)
	      li = atoi(argv[i+1]);
	    i++;
	  }
	  else if (strcmp(argv[i], "-fs") == 0){
	    fs = true;
	  }
	  else if (strcmp(argv[i], "-nm") == 0){
	    nm = true;
	  }
	  else
	    {
	      //zwar ein gueltiger Parameter, aber die Variable ist unbekannt
	      returnvalue = 1;
	      break;
	    }
	}
      else
	{
	  // wenn nicht, dann ist es kein Gueltiger parameter
	  returnvalue = 2;
	  break;
	}
    }
  return returnvalue;
}
