#include "openspace.h"
#include "spaceobjects.h"
#include "projectils.h"
#include "vector.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <SDL_rotozoom.h>
#include <SDL_image.h>

using namespace std;

map<string,SDL_Surface*> SpaceObject::LoadedImages;

SpaceObject::SpaceObject(const char* initfile, double px, double py, double mx, double my, double lx, double ly){
  lifetime = 0;
  pos.setXY(px,py);
  move.setXY(mx,my);
  look.setXY(lx,ly);
  look.setLength(1);
  char buffer[100];
  string cfg_line;
  string key;
  string value;
  string hitname = "hit-";
  string initpath = "data/spaceobjects/";
  initfile = initpath.append(initfile).c_str();
  //cout<<initpath<<endl;
  ifstream file (initfile, ios::in);
  map<string,SDL_Surface*>::iterator it;
  while (! file.eof() ) {
    file.getline (buffer,100);
    cfg_line = string(buffer);
    key = cfg_line.substr(0,cfg_line.find("=",0));
    value = cfg_line.substr(cfg_line.find("=",0)+1);
    
    if (0 == key.compare("gfx")){
      hitname = hitname.append(value).c_str();
      it = LoadedImages.find(value);
      if (it != LoadedImages.end()) {
	image = it->second;
	//cout<<"bild gefunden..."<<endl;
      } else {
	image = IMG_Load(value.c_str());
	if(NULL == image){
	  cout<<initfile<<endl;
	  cout<<"key ("<<key<<")"<<endl;
	  cout<<"Fehler beim Laden des Bildes ("<<value<<") : "<<SDL_GetError()<<endl;
	  exit(-1);
	}
	LoadedImages[value] = image;
	//cout<<"bild geladen..."<<endl;
      }
    }
    //if (another key)
  }
  //  SDL_SetColorKey(image, SDL_SRCCOLORKEY, SDL_MapRGB(image->format, 0, 0, 0)); 
  width = (image->w)/16;
  heigth = image->h;
  destination.w = width;
  destination.h = heigth;
  destination.x = (short int)px-width/2;
  destination.y = -(short int)py-heigth/2;
  source.w = width;
  source.h = heigth;
  source.x = 0;
  source.y = 0;
  turn(0);
  //guns = 1;
  //rockets = 10;
  //MaxGuns = 5;
  //MaxRockets = 50;
  MaxSpeed = 1000;
  mass = (width * heigth)/2 * 8;
  accel = 0;
  spin = 0;
  MaxEnergy = 5000;
  energy = 5000;
  damage = 0;
  nextaction = SDL_GetTicks();
  hidden = false;
  target = NULL;
  parent = NULL;
  score = 0;
  agility = 1.0;
  TrackedBy.clear();
  r = g = b = 255;

  gotHit = 0;

  // bild zur treffersignalisierung
  it = LoadedImages.find(hitname);
  if (it != LoadedImages.end()) {
    hit = it->second;
    //cout<<"bild gefunden..."<<endl;
  } else {
    hit = IMG_Load("data/graphics/hit.png");
    if(NULL == hit){
      cout<<initfile<<endl;
      cout<<"key ("<<key<<")"<<endl;
      cout<<"Fehler beim Laden des Bildes ("<<value<<") : "<<SDL_GetError()<<endl;
      exit(-1);  
    }
    //cout<<this->width/200.0;
    /*
      SDL_Surface *surface;
      surface = SDL_CreateRGBSurface(SDL_HWSURFACE, pic->w*16, pic->h, 32,
                                      rmask, gmask, bmask, amask);

    */
    hit = rotozoomSurface(hit, 0.0, this->width/200.0, SMOOTHING_ON);
    LoadedImages[hitname] = hit;
    //cout<<"bild geladen..."<<endl;
    }
  //  SDL_SetColorKey(hit, SDL_SRCCOLORKEY, SDL_MapRGB(hit->format, 0, 0, 0)); 
  //  SDL_SetAlpha(hit,SDL_SRCCOLORKEY,128);
  

  
  //  turbotime = SDL_GetTicks() - 300;
}
SpaceObject::~SpaceObject(){
  //  SDL_FreeSurface(image);
  if(NULL != target) {
    target->isNomoreTrackedBy(this);
    //    cout<<"target was: "<<(int)target % 1000<<endl;
  }

  list<SpaceObject*>::iterator it;
  it =  TrackedBy.begin();
  while(it !=  TrackedBy.end()){
    //    cout<<"was tracked by: "<<(int)(*it) % 1000<<endl;
    (*it)->unsetTarget((*it));
    it++;
  }
}


void SpaceObject::setTarget(SpaceObject* target){
  if(target != this->target) {
    if(NULL != this->target)
      this->target->isNomoreTrackedBy(this);
    this->target = target;
    if(NULL != target) {
      this->target->isTrackedBy(this);
    }
    //    cout<<(int)this % 1000<<" setTarget: "<<(int)target % 1000<<endl;
  }
}

void SpaceObject::isNomoreTrackedBy(SpaceObject* tracker){
  //  cout<<(int)this % 1000<<" isNomoreTrackedBy: ";
  list<SpaceObject*>::iterator it;
  it = TrackedBy.begin();
  while(it !=  TrackedBy.end()){
    if((*it) == tracker){
      tracker->unsetTarget(*it);
      it = TrackedBy.erase(it);
      //      cout<<(int)tracker % 1000<<endl;
      break;
    }
    it++;
    }
}

void SpaceObject::isTrackedBy(SpaceObject* tracker){
  list<SpaceObject*>::iterator it;
  it =  TrackedBy.begin();
  bool contains = false;
  while(it !=  TrackedBy.end()){
    if((*it) == tracker)
      contains = true;
    it++;
  }
  if(!contains){
    TrackedBy.push_back(tracker);
    //    cout<<(int)this % 1000<<" isTrackedBy: "<<(int)tracker % 1000<<endl;
  }
}

void SpaceObject::unsetTarget(SpaceObject* target){
  //  cout<<(int)this % 1000<<" unsetTarget: "<<(int)target % 1000<<endl;
  if(NULL != this->target)
    if(NULL == target) {
      this->target->isNomoreTrackedBy(this);
      this->target = NULL;
    }
    else if(target == this->target)
      this->target->isNomoreTrackedBy(this);      
      this->target = NULL;
}



SpaceObject* SpaceObject::nextTo(double distance){
  SpaceObject* toReturn = NULL;
  myVector direction; 
  
  list<SpaceObject*>::iterator it;
  it =  GlobalListOfSpaceObjects.begin();
  while(it !=  GlobalListOfSpaceObjects.end()){
    direction = ((*it)->pos + (*it)->move) - (this->pos - this->move);
    if((*it)->isTarget() && distance > ((*it)->pos - this->pos).getLength()){
      toReturn = (*it);
      distance = ((*it)->pos - this->pos).getLength();
    }
    it++;
  }
  return toReturn;
}


void SpaceObject::accelerate(double acc) {
  if(1 >=move.getLength())
    move += look * (acc * 2);
  else
    move += look * (acc / 2);
    //    move += look * (acc * 2 * (1.0 - move.getLength()/(MaxSpeed + 1)));
  if (MaxSpeed <= abs(move.getLength())){
    move.setLength(MaxSpeed);
  }
  if(0 < acc){
    Bullet* deto;
    myVector newdir, newpos;
    int speed = (int)acc/2 + 1; // sollte kleinergleich 100 sein
    int distance = speed/2 + 1; // mindestens 2
    newdir.setXY(-look.getX(),-look.getY());
    //newdir.turn(-M_PI/8);
    //look.turn(-M_PI/16);
    //for(int i=0; i<=2;i++){ 
      for(int i=0; i<speed/distance; i++) {
	newpos.setXY(getX()-look.getX()*(width+distance*i),
		     getY()-look.getY()*(heigth+distance*i));
	//newpos.setXY(getX()-look.getX()*(width/2+distance*i)+move.getX(),getY()-look.getY()*(heigth/2+distance*i)+move.getY());
	deto = new Bullet("drive.cfg",
			  newpos.getX(),
			  newpos.getY(),
			  newdir.getX()*speed+move.getX(),
			  newdir.getY()*speed+move.getY(),
			  0,0,1,10);
	GlobalListOfSpaceObjects.push_back(deto);
      }
      //  newdir.turn(M_PI/8);
      //look.turn(M_PI/16);
      //}
      //look.turn(-M_PI/8);
  }
}

/*void SpaceObject::turbo(){
  move += look * (20);
  turbotime = SDL_GetTicks();
  }*/

void SpaceObject::aim(SpaceObject &target){
  myVector direction;
  double distance;
  distance = (target.pos - this->pos).getLength();
  direction = (target.pos + target.move * 10) - (this->pos  + this->move * 10);
  double deltaAngle = direction.getAngle() - look.getAngle();
  if((M_PI/32 > abs(deltaAngle))){
    if(M_PI < abs(deltaAngle))
      if(0 < deltaAngle)
	this->spin = -abs(deltaAngle)*M_PI;
      else
	this->spin = abs(deltaAngle)*M_PI;
    else
      if(0 < deltaAngle)
	this->spin = abs(deltaAngle)*M_PI;
      else
	this->spin = -abs(deltaAngle)*M_PI;
    
  } else {
    if(M_PI*2 - M_PI/32 < abs(deltaAngle)){
      if(M_PI < abs(deltaAngle))
	if(0 < deltaAngle)
	  this->spin = -(M_PI*2-abs(deltaAngle))*M_PI;
	else
	  this->spin = (M_PI*2-abs(deltaAngle))*M_PI;
      else
	if(0 < deltaAngle)
	  this->spin = (M_PI*2-abs(deltaAngle))*M_PI;
	else
	  this->spin = -(M_PI*2-abs(deltaAngle))*M_PI;
      
    } else if(M_PI < abs(deltaAngle))
      if(0 < deltaAngle)
	this->spin = -1.0;
      else
	this->spin = 1.0;
    else
      if(0 < deltaAngle)
	this->spin = 1.0;
      else
	this->spin = -1.0;
  }
  //cout<<this->spin<<endl;
}

void SpaceObject::aim(myVector direction){
  double deltaAngle = direction.getAngle() - look.getAngle();
  if(M_PI/32 > abs(deltaAngle)){
    this->spin = 0.0;
  } else if(M_PI < abs(deltaAngle))
    if(0 < deltaAngle)
      this->spin = -1.0;
    else
      this->spin = 1.0;
  else
    if(0 < deltaAngle)
      this->spin = 1.0;
    else
      this->spin = -1.0;
}


void SpaceObject::turn(double ay) {
  look.turn(ay*M_PI/16); // hier steckt ein Wurm drin....
  //Folgende Formel berechnet aus dem Winkel die Position aus dem Quellbild...
  source.x = (short int)((int)((2-(look.getAngle()/M_PI+1))*8+0.5)*(image->w/16))%(image->w); //dont touch this Form!!
  //cout<<"newSource:"<<source.x<<endl;
  //cout<<"lookAngle:"<<look.getAngle()<<endl;
}
//void PlayerFigure::accelerate(int ax, int ay){
//  this->accelerateX(ax);
//  this->accelerateY(ay);
//}
int SpaceObject::makeMove(){
  lifetime++;
  --gotHit;
  if(0 >= energy)
    return 0;
  else {
    this->accelerate(this->accel);
    this->turn(this->spin*agility);
    pos += move;
    
    
    /**  urspr�nglich gedacht um an den w�nden abzuprallen...
     if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
     if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
     if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
     if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */
    
    
    destination.x = (short int)pos.getX()-width/2;
    
    destination.y = -((short int)pos.getY())-heigth/2;
     return 1;
  }
}

//int SpaceObjects::collision()

// todo: was heisst Stoss auf englisch?
// diese methode ist veraltet!!!! ab jetzt nur noch collision benutzen!!!
/*
void SpaceObject::elasticStoss(SpaceObject *hitmate){
  if(!(this->isTransparent() || hitmate->isTransparent()))
    if(!(this->isBullet() && hitmate->isBullet()))

    {

    if(this->isPowerup() || hitmate->isPowerup()){
      if(this->isSpaceShip() && hitmate->isPowerup()){
	hitmate->applyTo(this);	
      } else if(this->isPowerup() && hitmate->isSpaceShip()){
	this->applyTo(hitmate);
      } 
    } else {


      bool onedead = false;
      
      energy -= hitmate->damage;
      if(0 > energy){
	energy = 0; 
	onedead = true;
	Detonation* deto;
	deto = new Detonation("detonation-5.cfg",
			      this->pos.getX(),
			      this->pos.getY(),
			      0,
			      0, 8);
	GlobalListOfSpaceObjects.push_back(deto);
      }
      
      hitmate->energy -= damage;
      if(0 > hitmate->energy){
	hitmate->energy = 0; 
	onedead = true;
	Detonation* deto;
	deto = new Detonation("detonation-5.cfg",
			      hitmate->pos.getX(),
			      hitmate->pos.getY(),
			      0,
			      0, 8);
	GlobalListOfSpaceObjects.push_back(deto);
      }
      
      if(!onedead){
	myVector stossDir, stCompTh, stCompHm, fitCompTh, fitCompHm;
	
	stossDir.setXY(this->getX()-hitmate->getX(),this->getY()-hitmate->getY());
	
	stCompTh = this->move.proj(stossDir);
	stCompHm = hitmate->move.proj(stossDir);
	
	fitCompTh = this->move - stCompTh;
	fitCompHm = hitmate->move - stCompHm;
	
	double m1 = this->mass;
	double m2 = hitmate->mass;
	//cout<<"m1:"<<m1<<"should be:"<<this->mass<<endl;
	//cout<<"m2:"<<m2<<"should be:"<<hitmate.mass<<endl;
	
	//fitCompTh += stCompHm;
	//fitCompHm += stCompTh;
	
	fitCompTh += stCompTh * ((m1-m2)/(m1+m2)) + stCompHm *  ((2*m2)/(m1+m2));
	fitCompHm += stCompTh *  ((2*m1)/(m1+m2)) + stCompHm * ((m2-m1)/(m2+m1));
	
	this->move.setXY(fitCompTh.getX(),fitCompTh.getY());
	hitmate->move.setXY(fitCompHm.getX(),fitCompHm.getY());
      }
    }
  }
}
*/

bool SpaceObject::takeDamage(int damage){
  this->energy -= damage;
  if(0 > this->energy || 0 < this->damage){
    this->energy = -1; 
    return true;
  } else
    return false;
}



void SpaceObject::collision(SpaceObject *hitmate){
  bool wasdead = false;
  if(this->energy == -1 || hitmate->energy == -1){
    wasdead = true;
  }
  if(!(this->isTransparent() || hitmate->isTransparent()))
    if(!(this->isBullet() && hitmate->isBullet())) {
      if(this->isPowerup() || hitmate->isPowerup()){
	  if(this->isSpaceShip() && hitmate->isPowerup()){
	    hitmate->applyTo(this);
	  } else if(this->isPowerup() && hitmate->isSpaceShip()){
	    this->applyTo(hitmate);
	  } 
	} else {
	  if(hitmate->isMeta()){
	    //SpaceObject *newmate = this;
	    //cout<<"metaman"<<endl;
	    hitmate->collision(this);
	  } else
	    {
	  bool onedead = false;
	  
	  //cout<<"~";
	  onedead = this->takeDamage(hitmate->damage);
	  onedead = hitmate->takeDamage(this->damage) || onedead;


	  if(!wasdead){
	    if(this->isBullet()) hitmate->showHit();
	    if(hitmate->isBullet()) this->showHit();
	    
	    if(!onedead){

	  myVector hitpos;
	  hitpos = this->pos + (hitmate->pos - this->pos) * ((double)this->width / (double)(this->width + hitmate->width));
	  	  
	  Detonation* deto;
	  deto = new Detonation("detonation-5.cfg",
				hitpos.getX(),
				hitpos.getY(),
				0,
				0, 8);
	  GlobalListOfSpaceObjects.push_back(deto);
	  


	      myVector stossDir, stCompTh, stCompHm, fitCompTh, fitCompHm;
	      
	      stossDir.setXY(this->getX()-hitmate->getX(),this->getY()-hitmate->getY());
	      
	      stCompTh = this->move.proj(stossDir);
	      stCompHm = hitmate->move.proj(stossDir);
	      
	      fitCompTh = this->move - stCompTh;
	      fitCompHm = hitmate->move - stCompHm;
	      
	      double elastizitaet = 0.1;
	      double damagespeed = ((stCompTh - stCompHm).getLength()) * elastizitaet;
	      stCompTh.setLength(stCompTh.getLength()*(1 - elastizitaet));
	      stCompHm.setLength(stCompHm.getLength()*(1 - elastizitaet));
	      
	      //cout<<damagespeed<<endl;
	      
	      double m1 = this->mass;
	      double m2 = hitmate->mass;
	      
	      this->takeDamage((int)(m2 * (damagespeed * damagespeed)));
	      hitmate->takeDamage((int)(m1 * (damagespeed * damagespeed)));
	      
	      
	      
	      //cout<<"m1:"<<m1<<"should be:"<<this->mass<<endl;
	      //cout<<"m2:"<<m2<<"should be:"<<hitmate.mass<<endl;
	      
	      //fitCompTh += stCompHm;
	      //fitCompHm += stCompTh;
	      
	      fitCompTh += stCompTh * ((m1-m2)/(m1+m2)) + stCompHm *  ((2*m2)/(m1+m2));
	      fitCompHm += stCompTh *  ((2*m1)/(m1+m2)) + stCompHm * ((m2-m1)/(m2+m1));
	      
	      this->move.setXY(fitCompTh.getX(),fitCompTh.getY());
	      hitmate->move.setXY(fitCompHm.getX(),fitCompHm.getY());
	      
	      double leap = - ((hitmate->pos + hitmate->move) - (this->pos + this->move)).getLength() + (double)(this->width + hitmate->width)/2.0;
	      
	      
	      if(0 < leap ){
		double leapangle = ((hitmate->pos + hitmate->move) - (this->pos + this->move)).getAngle();
		double scale = leap / (this->move.getLength() + hitmate->move.getLength());
		myVector tmpMoveTh = this->move * scale;
		tmpMoveTh.setAngle(M_PI + leapangle);
		myVector tmpMoveHm = hitmate->move * scale;
		tmpMoveHm.setAngle(leapangle);
		this->pos += tmpMoveTh;
		hitmate->pos += tmpMoveHm;
		
	      }
	    }
	    if (this->energy == -1 && hitmate->parent != NULL) {
	      hitmate->parent->score += this->outScore();
	    }
	    if (hitmate->energy == -1 && this->parent != NULL) {
	      this->parent->score += hitmate->outScore();
	    }
	  }
	}
      }
  }
}

bool SpaceObject::collisionsCheck(SpaceObject *hitmate){
  bool returnvalue = false;
  myVector *Distance = new myVector;
  Distance->setXY(this->getPos().getX()-hitmate->getPos().getX(),this->getPos().getY()-hitmate->getPos().getY());
  if(Distance->getLength() <= this->getWidth()/2.0 + hitmate->getWidth()/2.0){
    returnvalue = true;
  
    /*
  myVector hitpos;
  hitpos = this->pos + (hitmate->pos - this->pos) * ((double)this->width / (double)(this->width + hitmate->width));
  
  Detonation* deto;
  deto = new Detonation("detonation-5.cfg",
			hitpos.getX(),
			hitpos.getY(),
			0,
			0, 8);
  GlobalListOfSpaceObjects.push_back(deto);
    */
  } 
  delete Distance;
  return returnvalue;
}


void SpaceObject::show(SDL_Surface *screen, int x, int y){
  if(!hidden) {
    SDL_Rect dest = destination;
    dest.x -= x;
    dest.y -= y;    
    SDL_BlitSurface(image, &source, screen, &dest);
    if(0 < gotHit) {
      dest = destination;
      dest.x -= x;
      dest.y -= y;    
      SDL_BlitSurface(hit, NULL, screen, &dest);
      //cout<<"hitme"<<endl;
    }
  }
}

void SpaceObject::showHit(){
  gotHit = 2;
}


void SpaceObject::indicate_direction(int px, int py, SpaceObject &target, SDL_Surface *screen){
  myVector dir;
  dir.setXY(target.getX()-this->getX(),-target.getY()+this->getY());
  dir.setLength(dir.getLength()/10);
  SDL_Rect draw;
  draw.x = px + (int)dir.getX(); 
  draw.y = py + (int)dir.getY(); 
  draw.w = 2;
  draw.h = 2;
  SDL_FillRect(screen, &draw, SDL_MapRGB(screen->format, 255, 255, 255));
}

void SpaceObject::indicate(SDL_Surface *screen, int rel_x, int rel_y, int range_x, int range_y){
  if(this->isTarget() || this->isPlayer() || isPowerup()){
    SDL_Rect draw;
    draw.x = (getX() -rel_x +range_x/2 -this->width/2) * (screen->w) / (range_x) -1; 
    draw.y = -((getY()) -rel_y -range_y/2 +this->heigth/2) * (screen->h) / (range_y) -1; 
    draw.w = this->width * (screen->w) / (range_x) +2; 
    draw.h = this->heigth * (screen->h) / (range_y) +2; 
    if(0 < gotHit) {
      SDL_FillRect(screen, &draw, SDL_MapRGB(screen->format, 191,0,0));
    }else
    SDL_FillRect(screen, &draw, SDL_MapRGB(screen->format, r,g,b));

  }
}

void SpaceObject::showbar(int px, int py, int wi, int he, int value, int maxval, SDL_Surface *screen, int fr, int fg, int fb, int ovr, int ovg, int ovb, int tr, int tg, int tb){
  int r,g,b;
  SDL_Rect dest;
  
  dest.x = px;
  dest.y = py;
  dest.w = wi;
  dest.h = he;

  if(0 > px)
    dest.x = 0;
  else
    px = 0;
  if( 0 > py)
    dest.y = 0;
  else
    py = 0;
  dest.w = wi + px;
  dest.h = he + py;

  if (0 != maxval) {
    if (maxval/2.0 > value){
      r = (int)(fr * (maxval - value)/(maxval/2.0)  + ovr * value/(maxval/2.0));
      g = (int)(fg * (maxval - value)/(maxval/2.0)  + ovg * value/(maxval/2.0));
      b = (int)(fb * (maxval - value)/(maxval/2.0)  + ovb * value/(maxval/2.0));
    } else {
      r = (int)(ovr * (maxval - value)/(maxval/2.0)  + tr * value/(maxval/2.0));
      g = (int)(ovg * (maxval - value)/(maxval/2.0)  + tg * value/(maxval/2.0));
      b = (int)(ovb * (maxval - value)/(maxval/2.0)  + tb * value/(maxval/2.0));
    }
  
        
    if (px > -wi && py > -he){
      SDL_FillRect(screen, &dest, SDL_MapRGB(screen->format, 0, 0, 0));
    }
    if ((value*wi/maxval) + px > 0 && py > -he){
      dest.w = (value*wi/maxval) + px;
      SDL_FillRect(screen, &dest, SDL_MapRGB(screen->format, r, g, b));
    }
  }
  else {
    r = tr;
    g = tg;
    b = tb;
    dest.w = wi + px;
    SDL_FillRect(screen, &dest, SDL_MapRGB(screen->format, r, g, b));
  }
}

void SpaceObject::showint(int px, int py, int wi, int he, int value, SDL_Surface *screen){
  if(!TTF_WasInit() && -1 == TTF_Init()) {
    printf("TTF_Init: %s\n", TTF_GetError());
    exit(1);
  }
  TTF_Font *font;
  char* font_file = "data/Neuropol.ttf";
  int size = he;
  if( (font = TTF_OpenFont(font_file, size)) == NULL) {
    printf("TTF_OpenFont error: %s", TTF_GetError());
    exit (-1);
  }
  SDL_Color text_color;
  text_color.r = 0xff;
  text_color.g = 0xff;
  text_color.b = 0xff;
  SDL_Rect dest;
  dest.x = px;
  dest.y = py;
  dest.w = wi;
  dest.h = he;
  SDL_Surface *text_surf;
  
  stringstream scrstream;
  scrstream<<value;
   
  text_surf = TTF_RenderText_Blended(font, scrstream.str().c_str(), text_color);
  dest.x = dest.x + dest.w - text_surf->w -1;
  SDL_BlitSurface(text_surf, NULL, screen, &dest);      
  SDL_FreeSurface(text_surf);
  TTF_CloseFont(font);
}


void SpaceObject::showenergy(int px, int py, int wi, int he, SDL_Surface *screen){
  showbar(px, py, wi, he, energy, MaxEnergy, screen);
}





MetaSpaceObject::MetaSpaceObject(const char* initfile, double px, double py, double mx, double my, double lx, double ly):
  SpaceObject(initfile, px, py, mx, my, lx, ly){
  ListOfSpaceObjects.clear();
  Player = false;
  Enemy = false;
  EnemyShip = false;
  SpaceShip = false;
  Transparent = false;
  Target = false;
  Powerup = false;
  Bullet = false;
  Asteorid = false;

}

MetaSpaceObject::~MetaSpaceObject(){

  list<SpaceObject*>::iterator it;
  
  it =  ListOfSpaceObjects.begin();
  while(it !=  ListOfSpaceObjects.end()){
    delete (*it);
    it = ListOfSpaceObjects.erase(it);
  }
  
  
}

void MetaSpaceObject::addObject(SpaceObject *newObject){
  if (ListOfSpaceObjects.begin() == ListOfSpaceObjects.end()){
    this->pos.setXY(newObject->getX(),newObject->getY());
    this->width = newObject->getWidth();
    this->heigth = newObject->getHeigth();
    this->move = newObject->move;
  }
  else{
    
    double distance;
    

    distance = (this->pos - newObject->pos).getLength();
    
    if(distance * 2 + newObject->getWidth()/2> this->width){
      this->width = (int)distance * 2 + newObject->getWidth();
      this->heigth = (int)distance * 2 + newObject->getWidth();
    }

      //cout<<this->width<<endl;


  }
    
  //this->damage += newObject->damage;
  ListOfSpaceObjects.push_back(newObject);

}


int MetaSpaceObject::makeMove(){
  
  list<SpaceObject*>::iterator it;
  
  //cout<<"move Ya"<<endl;
  it =  ListOfSpaceObjects.begin();
  while(it !=  ListOfSpaceObjects.end()){
    //cout<<" einer"<<endl;
    if(!(*it)->makeMove()){
      //cout<<"  tot"<<endl;
      delete (*it);
      it = ListOfSpaceObjects.erase(it);
    } else {
      if(it != ListOfSpaceObjects.end()){
	it++;
      } else {
	//cout<<"war letzer"<<endl;
	break;
      }
    }
  }

  lifetime++;
  if(ListOfSpaceObjects.begin() == ListOfSpaceObjects.end()){
    return 0;
  }
  else {
    
    this->accelerate(this->accel);
    this->turn(this->spin);
    pos += move;
    
    destination.x = (short int)pos.getX()-width/2;
    
    destination.y = -((short int)pos.getY())-heigth/2;
    
    return 1;
  }

}
void MetaSpaceObject::show(SDL_Surface *screen, int x, int y){
  list<SpaceObject*>::iterator it;

  it =  ListOfSpaceObjects.begin();
  while(it !=  ListOfSpaceObjects.end()){
    (*it)->show(screen, x, y);
    ++it;
  }

}
void MetaSpaceObject::elasticStoss(SpaceObject *hitmate){}
void MetaSpaceObject::collision(SpaceObject *hitmate){

  list<SpaceObject*>::iterator it;
  //cout<<"alle checken"<<endl;
  //myVector *Distance = new myVector;

  /*
  bool hit = false;

  it =  ListOfSpaceObjects.begin();
  while(it !=  ListOfSpaceObjects.end()){
    if((*it)->collisionsCheck(hitmate)){
      hit = true;
    }
    it++;
  }
  */


  it =  ListOfSpaceObjects.begin();
  //cout<<"+";
  while(it !=  ListOfSpaceObjects.end()){
    //cout<<" einen cheken"<<endl;
    //Distance->setXY((*it)->getPos().getX()-hitmate->getPos().getX(),(*it)->getPos().getY()-hitmate->getPos().getY());
    //if(Distance->getLength() <= (*it)->getWidth()/2.0 + hitmate->getWidth()/2.0){
    //if((*it)->collisionsCheck(hitmate) || (hit && (*it)->isBullet()) || true){
    //cout<<"*";
      (*it)->collision(hitmate);
      //}
      //cout<<"-";
    //cout<<"  treffer"<<endl;
    //}
    it++;
  }
  //cout<<"#"<<endl;

  //delete Distance;
}

void MetaSpaceObject::applyTo(SpaceObject *target){}
bool MetaSpaceObject::takeDamage(int damage){
  bool returnvalue = false;
  list<SpaceObject*>::iterator it;
  //cout<<"und..."<<endl;
  it =  ListOfSpaceObjects.begin();
  while(it !=  ListOfSpaceObjects.end()){
    returnvalue = (*it)->takeDamage(damage) || returnvalue;
    //cout<<this->damage<<" / "<<damage<<endl;
    it++;
  }
  return returnvalue;
  
}

bool MetaSpaceObject::collisionsCheck(SpaceObject *hitmate){
  bool returnvalue = false;
  list<SpaceObject*>::iterator it;
  
  it =  ListOfSpaceObjects.begin();
  while(it !=  ListOfSpaceObjects.end()){
    returnvalue = returnvalue || (*it)->collisionsCheck(hitmate);
    
    it++;
  }
  return returnvalue;
  
}







Asteorid::Asteorid(const char* initfile, double px, double py, double mx, double my, double lx, double ly, int inst):
  SpaceObject(initfile, px, py, mx, my, lx, ly){
  instanz = inst;
  r = 106;
  g = 57;
  b = 37;
  mass = (width * heigth)/2 * 32;
  MaxEnergy = mass * 2;
  energy = mass * 2;
  
}

Asteorid::~Asteorid(){
  Asteorid* asteo;
  int anzahl = 0;
  myVector newpos;
  myVector newspeed;
  string newcfg;
  newpos.setLength((instanz-2)*3);
  newspeed.setLength(0.5);
  int x = (instanz)/2;
  if(instanz <= x){
    x = instanz-1;
  }
  for(;instanz>x;--instanz){
  if (instanz == 6) {
    anzahl = 2;
    newcfg = "asteroid5.cfg";
  }
  if (instanz == 5) {
    anzahl = 3;
    newcfg = "asteroid4.cfg";
  }
  if (instanz == 4) {
    anzahl = 3 + (random()%1);
    newcfg = "asteroid3.cfg";
  }
  if (instanz == 3) {
    anzahl = 3 + (random()%2);
    newcfg = "asteroid2.cfg";
  }
  if (instanz == 2) {
    anzahl = 3 + (random()%3);
    newcfg = "asteroid1.cfg";
  }
  if (instanz == 1) {
    anzahl = 3 + (random()%4);
    newcfg = "asteroid0.cfg";
  }
  if (4 == instanz){
    Powerup* pup;
    pup = new Powerup("pwrup_ore.cfg",
		      this->getX(),
		      this->getY(),
		      this->move.getX(),
		      this->move.getY(),
		      8);
    //GlobalListOfSpaceObjects.push_back(pup);
  }
  if(0 < anzahl){
    asteo = new Asteorid(newcfg.c_str(),
			 this->getX(),
			 this->getY(),
			 this->move.getX(),
			 this->move.getY(),
			 -1.0,0.0, instanz-1);
    GlobalListOfSpaceObjects.push_back(asteo);
  }
  for(int i=1; i<anzahl;i++){
    newpos.turn(2*M_PI/anzahl);
    newspeed.turn(2*M_PI/anzahl);
    asteo = new Asteorid(newcfg.c_str(),
			 this->getX()+newpos.getX(),
			 this->getY()+newpos.getY(),
			 this->move.getX()+newspeed.getX(),
			 this->move.getY()+newspeed.getY(),
			 -1.0,0.0, instanz-1);
    GlobalListOfSpaceObjects.push_back(asteo);
  }
 }
}

Detonation::Detonation(const char* initfile, double px, double py, double mx, double my, int dur):
  SpaceObject(initfile, px, py, mx, my, -1.0, 0.0){
  //  energy = 10000000;
  //damage = this->width * 10;
  expire = dur;
  i = 1;
}

Detonation::~Detonation(){
}

int Detonation::makeMove(){
  lifetime++;
  this->i++;
  this->accelerate(this->accel);
  this->turn(-2);
  pos += move;

      
  if(this->expire < this->i){
    return 0;
  }

  destination.x = (short int)pos.getX()-width/2;
  destination.y = -((short int)pos.getY())-heigth/2;

  return 1;
}


