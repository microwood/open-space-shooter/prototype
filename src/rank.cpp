#include "rank.h"

Rank::Rank(){
  image = IMG_Load("data/graphics/rank-marine.png");
  stufe = 0;
  laufbahn = 3;

  destination.w = 30;
  destination.h = 12;
  destination.x = 0;
  destination.y = 0;
  source.w = destination.w;
  source.h = destination.h;
  source.x = 0;
  source.y = 0;
}

void Rank::show(SDL_Surface *screen, int xpos, int ypos){
  // Auswahl des Richtigen Bildes aus dem Laufbahnbild
  source.x = destination.w * stufe;
  source.y = destination.h * laufbahn;
  // Festlegen der Zielposition
  destination.x = xpos;
  destination.y = ypos;
  // Anzeigen des Bildes
  SDL_BlitSurface(image, &source, screen, &destination);
}

bool Rank::incRank(){
  bool returnvalue = false;
  switch(laufbahn){
  case 0: // Manschaften
    if(5 > stufe) {
      stufe++;
      returnvalue = true;
    }
    break;
  case 1: // Unteroffiziere
    if(7 > stufe) {
      stufe++;
      if(3 <= stufe && stufe <= 5)
	stufe = 6;
      returnvalue = true;
    }      
    break;
  case 2: // Feldwebel
    if(12 > stufe) {
      stufe++;
      if(3 <= stufe && stufe <= 5)
	stufe = 6;
      if(7 == stufe)
	stufe = 8;
      returnvalue = true;
    }
    
    break;    
  case 3: // Offizier
    if(24 > stufe) {
      stufe++;
      if(3 <= stufe && stufe <= 5)
	stufe = 6;
      if(7 == stufe)
	stufe = 8;
      if(9 == stufe)
	stufe = 10;
      if(11 <= stufe && stufe <= 12)
	stufe = 13;
      returnvalue = true;
    }
    break; 
  default: 
    //cout<<"irgendwas l�uft schief..."<<endl;
    break;
  }
  return returnvalue;
}

void Rank::decRank(){
  --stufe;
}

int Rank::getRank(){
  return stufe;
}

int Rank::getLaufbahn(){
  return laufbahn;
}

void Rank::setRank(int newR){

}

bool Rank::setLaufbahn(int newL){
  bool returnvalue = false;
  switch(newL){
  case 0: // Manschaften
    if(5 >= stufe){
      laufbahn = 0;
      returnvalue = true;
    }
    break;
  case 1: // Unteroffiziere
    if(7 >= stufe){
      laufbahn = 1;
      returnvalue = true;
    }
    break;
  case 2: // Feldwebel
    if(12 >= stufe){
      laufbahn = 2;
      returnvalue = true;
    }
    break;
  case 3: // Offiziere
    laufbahn = 3;
    returnvalue = true;
    break;
  default: 
    //cout<<"irgendwas l�uft schief..."<<endl;
    break;
  }
  return returnvalue;
}
