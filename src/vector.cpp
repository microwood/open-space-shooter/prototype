#include <iostream>
#include <math.h>
#include "vector.h"
//#include <strstream>

using namespace std;


myVector::myVector():
  posx(0), posy(0), length(0), angle(0), polar(false){}

myVector::~myVector(){}

void myVector::toScreen(){
  cout<<"("<<getX()<<","<<getY()<<") / "<<getLength()<<" *e^ "<<getAngle()<<" - "<<(polar?"Polarform":"Koordinatenform")<<endl;
}

/*
char* myVector::toString(){
  strstream output;
  output<<"("<<getX()<<","<<getY()<<")";
  return output.str();
}
*/

void putDot(SDL_Surface *screen, int x, int y, int r, int g, int b){
  SDL_Rect draw;
  draw.x = x -1; 
  draw.y = y -1; 
  draw.w = 3;
  draw.h = 3;
  SDL_FillRect(screen, &draw, SDL_MapRGB(screen->format, r,g,b));
}

void myVector::show(SDL_Surface *screen){
  int x, y;

  x = screen->w / 2;
  y = screen->h / 2;

  putDot(screen, x, y, 255,255,0);

  x = screen->w / 2;
  y = screen->h / 2;
  
  putDot(screen, x+(int)this->getX()*5, y-(int)this->getY()*5, 0,255,0);
  return;
}

void myVector::enablePolar(){
  if(!polar){
    length = sqrt(posx*posx+posy*posy);
    if(0 == length) {
      angle = 0;
    } else {
      angle = acos(posx/length);
      if(0 > posy){
	angle = -angle;
      }
    }
    polar = true;
  }
}

void myVector::disablePolar(){
  if(polar){
    posx = length*cos(angle);
    posy = length*sin(angle);
    polar = false;
  }
}

void myVector::fitAngle(){
  if(polar){
    while(M_PI < angle){
      angle -= 2*M_PI;
    }
    while(-M_PI >= angle){
      angle += 2*M_PI;
    }
  }
}

void myVector::setAngle(double newang){
  if(!polar){enablePolar();}
  angle = newang;
  fitAngle();
}

void myVector::setLength(double newlen){
  if(!polar){enablePolar();}
  length = newlen;
  // todo: was machen bei einer negativen Laenge;  
}


void myVector::setX(double newx){
  if(polar){disablePolar();}
  posx = newx;
}

void myVector::setY(double newy){
  if(polar){disablePolar();}
  posy = newy;
}

void myVector::setXY(double newx, double newy){
  setX(newx);
  setY(newy);
}

void myVector::setValue(myVector newv){
  setXY(newv.getX(),newv.getY());
}


void myVector::turn(double ang){
  if(!polar){enablePolar();}
  angle += ang;
  fitAngle();
}

myVector myVector::proj(myVector dest){
  myVector toReturn;
  double destAngle = dest.getAngle();
  toReturn.setXY(this->getX(),this->getY());
  toReturn.turn(-destAngle);
  toReturn.setY(0);
  toReturn.turn(destAngle);
  return toReturn;
}

/*
myVector* myVector::minus(myVector sub){
  myVector* toReturn = new myVector();
  toReturn->setXY(this->getX()-sub.getX(),this->getY()-sub.getY());
  return toReturn;
}
*/

double myVector::getLength(){
  if(polar){
    return length;
  } else {
    return sqrt(posx*posx+posy*posy);
  }
}

double myVector::getAngle(){
  if(polar){
    return angle;
  } else {
    double length = getLength();
    if(0 == length) {
      return 0;
    } else {
      angle = acos(posx/length);
      if(0 > posy){
	angle = -angle;
      }
      return angle;
    }
  }
}

double myVector::getX(){
  if(polar){
    return length*cos(angle);
  } else {
    return posx; 
  }
}

double myVector::getY(){
  if(polar){
    return length*sin(angle);
  } else {
    return posy;
  }
}

//string myVector::operator<<(){
//  return "("<<getX()<<","<<getY()<<") / "<<getLength()<<" *e^ "<<getAngle()<<" - "<<polar;
//}


myVector operator+(myVector vector1, myVector vector2){
  myVector toReturn;
  if(vector1.polar) {vector1.disablePolar();}
  if(vector2.polar) {vector2.disablePolar();}
  toReturn.setXY(vector1.posx + vector2.posx,vector1.posy + vector2.posy);
  return toReturn;
}

myVector operator-(myVector vector1, myVector vector2){
  myVector toReturn;
  if(vector1.polar) {vector1.disablePolar();}
  if(vector2.polar) {vector2.disablePolar();}
  toReturn.setXY(vector1.posx - vector2.posx,vector1.posy - vector2.posy);
  return toReturn;
}

myVector operator*(int skalar, myVector vector){
  myVector toReturn = vector;
  if(toReturn.polar){
    toReturn.length *= skalar;
  } else {
    toReturn.posx *= skalar;
    toReturn.posy *= skalar;
  }
  return toReturn;
}

myVector operator*(double skalar, myVector vector){
  myVector toReturn = vector;
  if(toReturn.polar){
    toReturn.length *= skalar;
  } else {
    toReturn.posx *= skalar;
    toReturn.posy *= skalar;
  }
  return toReturn;
}

myVector operator*(myVector vector, int skalar){
  return skalar * vector;
}

myVector operator*(myVector vector, double skalar){
  return skalar * vector;
}


myVector& myVector::operator+=(myVector summand){
  if(polar){disablePolar();}
  if(summand.polar) {summand.disablePolar();}
  posx += summand.posx;
  posy += summand.posy;

  return *this;
}

myVector& myVector::operator-=(myVector summand){
  if(polar){disablePolar();}
  if(summand.polar) {summand.disablePolar();}
  posx -= summand.posx;
  posy -= summand.posy;

  return *this;
}


myVector& myVector::operator*=(const double skalar){
  if(polar){
    length *= skalar;
  } else {
    posx *= skalar;
    posy *= skalar;
  }
  
  // todo: was ist wenn die Laenge Negativ wird?
  return *this;
}

