#include <iostream>
#include <sstream>
#include "openspace.h"
#include "projectils.h"
#include "messages.h"

const int tickIntervall = 60;

using namespace std;


SDL_Rect gamemapdim;

list<SpaceObject*> GlobalListOfSpaceObjects;
list<PlayerShip*> GlobalListOfPlayerShips;
list<SpaceObject*> GlobalListOfPassiveObjects;
map<int,list<SpaceObject*> > CollisionsMap;

bool myPause;


static Uint32 next_time;

Uint32 time_left(void)
{
  Uint32 now;
  
  now = SDL_GetTicks();
  if(next_time <= now)
    return 0;
  else
    return next_time - now;
}

CollisionsSector::CollisionsSector(){
  //bla
}


SpaceSector::SpaceSector(){
  //this->x = x;
  //this->y = y;

  // hintergrundbild laden:
  //background = IMG_Load("spaceart.jpeg");
  background = IMG_Load("data/graphics/background.jpeg");
  if(NULL == background){
    cout<<"Fehler beim Laden des Bildes\"10142wp8i2la9fuyam.jpg\": "<<SDL_GetError()<<endl;
  }
  gamemap = IMG_Load("data/graphics/background.jpeg");
  SDL_SetColorKey(gamemap, SDL_SRCCOLORKEY, SDL_MapRGB(gamemap->format, 0, 0, 0)); 
  


  // initialisierung der SpielKarte:
  gamemapdim.x = 0;
  gamemapdim.y = 0;
  gamemapdim.w = background->w;
  gamemapdim.h = background->h;

  target_dir = new SpaceObject("target-enemy-dir.cfg");

}

SpaceSector::~SpaceSector(){


}

SDL_Surface* SpaceSector::draw(int x, int y){

  // Hintergrund auf die Spielfaeche kopieren:
  SDL_BlitSurface(background, NULL, gamemap, NULL);
  //SDL_FillRect(background, NULL, 0);// (Uint32)(y*1000+x*10000+100000));//random());

  // Anzeigen...
  list<SpaceObject*>::iterator it;
  it =  GlobalListOfSpaceObjects.begin();
  while(it !=  GlobalListOfSpaceObjects.end()){
    (*it)->show(gamemap, x*gamemapdim.w, y*gamemapdim.h);
    it++;
  }

  //SDL_BlitSurface(gamemap, &target, status, NULL);
  it =  GlobalListOfPassiveObjects.begin();
  while(it !=  GlobalListOfPassiveObjects.end()){
    (*it)->show(gamemap, x*gamemapdim.w, y*gamemapdim.h);
    it++;
  }


  return gamemap;
}


OpenSpace::OpenSpace(SDL_Surface *screen, int level, Player *player){

  SDL_EnableUNICODE(1);
  SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);



  this->level = level;
  this->player = player;
  //  Uint8 *keys;

  Uint32 rmask, gmask, bmask, amask;

  /* SDL interprets each pixel as a 32-bit number, so our masks must depend
     on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif

  this->screen = screen;


  cursor = new SpaceObject("cursor.cfg");


  statusframe = IMG_Load("data/graphics/statusbar.png");
  if(NULL == statusframe){
    cout<<"Fehler beim Laden des Bildes\"statusbar.png\": "<<SDL_GetError()<<endl;
  }
  //  SDL_SetColorKey(statusframe, SDL_SRCCOLORKEY, SDL_MapRGB(statusframe->format, 0, 0, 0)); 

  minimap = IMG_Load("data/graphics/minimap.bmp");
  bigmap = IMG_Load("data/graphics/bigmap.bmp");

  /*
  bg1 = IMG_Load("bg1.jpeg");
  SDL_SetColorKey(bg1, SDL_SRCCOLORKEY, SDL_MapRGB(bg1->format, 0, 0, 0)); 
  bg2 = IMG_Load("bg2.jpeg");
  SDL_SetColorKey(bg2, SDL_SRCCOLORKEY, SDL_MapRGB(bg2->format, 0, 0, 0)); 
  bg3 = IMG_Load("bg3.jpeg");
  SDL_SetColorKey(bg3, SDL_SRCCOLORKEY, SDL_MapRGB(bg3->format, 0, 0, 0)); 
  bg4 = IMG_Load("bg4.jpeg");
  SDL_SetColorKey(bg4, SDL_SRCCOLORKEY, SDL_MapRGB(bg4->format, 0, 0, 0)); 
  */
  
  
  status = IMG_Load("data/graphics/statusbar.bmp");
  
  //sector = new SpaceSector();
  //Coordinate newsector;
  //newsector.x = 0;
  //newsector.y = 0;
  //sectors[0][0](0,0);
  //sectors[1][0] = new SpaceSector(1,0);


  /*
  miniground = IMG_Load("10142wp8i2la9fuyam-small.jpg");
  if(NULL == background){
    cout<<"Fehler beim Laden des Bildes\"10142wp8i2la9fuyam-small.jpg\": "<<SDL_GetError()<<endl;
  }
  minimap = IMG_Load("10142wp8i2la9fuyam-small.jpg");
  minimapdest.x = DISPLAY_W/2 - minimap->w/2;
  minimap_frame.x = DISPLAY_W/2 - minimap->w/2 -2;
  //minimapdest.x = 0;
  minimapdest.y = DISPLAY_H   - minimap->h;
  minimap_frame.y = DISPLAY_H   - minimap->h -2;
  minimapdest.w = minimap->w;
  minimapdest.h = minimap->h;
  minimap_frame.w = minimap->w +4;
  minimap_frame.h = minimap->h +4;
  */


  mainscreen.x = 100;
  mainscreen.y = 0;
  statusscreen.x = 0;
  statusscreen.y = 0;
  viewport_nw.w = viewport_n.w = viewport_no.w = viewport_w.w = viewport_m.w = viewport_o.w = viewport_sw.w = viewport_s.w = viewport_so.w = DISPLAY_W - 100;
  viewport_nw.h = viewport_n.h = viewport_no.h = viewport_w.h = viewport_m.h = viewport_o.h = viewport_sw.h = viewport_s.h = viewport_so.h = DISPLAY_H;

  bgview1.w = DISPLAY_W - 100;
  bgview1.h = DISPLAY_H;
  bgview2.w = DISPLAY_W - 100;
  bgview2.h = DISPLAY_H;
  bgview3.w = DISPLAY_W - 100;
  bgview3.h = DISPLAY_H;
  bgview4.w = DISPLAY_W - 100;
  bgview4.h = DISPLAY_H;

  target.w = 120;
  target.h = 120;
}

OpenSpace::~OpenSpace(){
}

void OpenSpace::reset(int level){

  if(NULL == player->ship){
    player->ship = new PlayerShip("player1.cfg",0.0,0.0,0.0,0.0, -1.0,0.0,1000); // Initialisierung SpielFigur 1
    player->ship->MaxEnergy = 5000 + 1000 * player->rank.getRank();
    player->ship->energy = 5000 + 1000 * player->rank.getRank();
    player->ship->MaxShield = 5000 + 1000 * player->rank.getRank();
    player->ship->shield = 5000 + 1000 * player->rank.getRank();
    player->ship->setGuns(1 + player->rank.getRank()/2);
    player->ship->setRockets(5 * player->rank.getRank());
    player->ship->score = player->score;
  }

  //player->ship = new PlayerShip("player1.cfg",0.0,0.0,0.0,0.0, -1.0,0.0,1000); // Initialisierung SpielFigur 1


  this->level = level;
  GlobalListOfPlayerShips.clear();
  GlobalListOfSpaceObjects.clear();

  StarBase* sb2 = new StarBase("medium_base.cfg", 0, 0);
  GlobalListOfSpaceObjects.push_back(sb2);
  

  PlayerShip* pl2;
  for(int i=1;i<=0;i++){
    pl2 = new PlayerShip("player2.cfg",random() % gamemapdim.w,-(random() % gamemapdim.h)
			 ,0.0,0.0, -1.0,0.0,1000, 1); // Initialisierung SpielFigur 1
    GlobalListOfPlayerShips.push_back(pl2);
    GlobalListOfSpaceObjects.push_back(pl2);
  }
  GlobalListOfPassiveObjects.clear();

  this->player->ship->setX(((random()%2)*2-1)*77);
  this->player->ship->setY(((random()%2)*2-1)*77);
  this->player->ship->look = this->player->ship->pos;
  this->player->ship->look.setLength(1.0); 
  GlobalListOfPlayerShips.push_back(this->player->ship);
  GlobalListOfSpaceObjects.push_back(this->player->ship);
  GlobalListOfPassiveObjects.push_back(this->player->ship->targetIndicator);
  GlobalListOfPassiveObjects.push_back(this->player->ship->target_dir);
  

  Asteorid*  asteo;
  myVector newmove;
  newmove.setLength(1.0);        //random() % gamemapdim.w
  int newx, newy;
  int size = 10;
  int dest = 20; //standart: 100
  for(int i=1; i<(3*size*size*dest)/100;i++){  //-(random() % gamemapdim.h)
    newmove.turn(random()%10);
    //asteo = new Asteorid("asteroid1.cfg",50.0+40-((level/2+i)%2)*80,-(gamemapdim.h)*i/anzahl
    //		 ,  newmove.getX(), newmove.getY(),-1.0,0.0,1250, 1250,1); // Initialisierung der Asteroiden
    newx = -gamemapdim.w *size +random()%(gamemapdim.w * size*2);
    newy = +gamemapdim.h *size -(random()%(gamemapdim.h * size*2));
    if(!(-gamemapdim.w < newx && newx < gamemapdim.w && -gamemapdim.h < newy && newy < gamemapdim.h) && (true)){
      if(!(random()%4)){
	asteo = new Asteorid("asteroid6.cfg", newx, newy, newmove.getX()*(random()%10), newmove.getY()*(random()%10),-1.0,0.0, 6); // Initialisierung der Asteroiden	
      } else {
	if(!(random()%4)) {
	  asteo = new Asteorid("asteroid6.cfg", newx, newy, newmove.getX(), newmove.getY(),-1.0,0.0, 6); // Initialisierung der Asteroiden
	} else if (!(random()%4)){
	  asteo = new Asteorid("asteroid5.cfg", newx, newy, newmove.getX(), newmove.getY(),-1.0,0.0, 5); // Initialisierung der Asteroiden
	} else if (!(random()%4)){
	  asteo = new Asteorid("asteroid4.cfg", newx, newy, newmove.getX(), newmove.getY(),-1.0,0.0, 4); // Initialisierung der Asteroiden
	} else {
	  asteo = new Asteorid("asteroid3.cfg", newx, newy, newmove.getX(), newmove.getY(),-1.0,0.0, 3); // Initialisierung der Asteroiden 
	}
	asteo->energy = -1;
      }
      GlobalListOfSpaceObjects.push_back(asteo);
    }
  }

  EnemyShip* enemy1;
  if (level%4){
    int anzahl = level * 2;
    anzahl++;
    newmove.setLength(0);
    for(int i=1; i<anzahl * 1;i++){
      newmove.turn(1234); // Zufall???
      if(i%3){
	//enemy1 = new EnemyShip("enemy1.cfg",1,300,-(gamemapdim.h)*i/anzahl
	//		     ,  newmove.getX(), newmove.getY(),0.0, 1.0, 1000); // Initialisierung der Gegner
	enemy1 = new EnemyShip("enemy1.cfg",
			       1,
			       
			       this->player->ship->look.getX() * gamemapdim.w*3*2
			       -gamemapdim.w*3 +(random() % gamemapdim.w*3*2),
			       this->player->ship->look.getY() * gamemapdim.h*3*2
			       +gamemapdim.h*3 -(random() % gamemapdim.h*3*2)
			       ,  newmove.getX(), newmove.getY(),0.0, 1.0, 1000); // Initialisierung der Gegner
      } else {
	//enemy1 = new EnemyShip("enemy2.cfg",2,250,-(gamemapdim.h)*i/anzahl
	//	       ,  newmove.getX(), newmove.getY(),0.0, 1.0, 1000); // Initialisierung der Gegner
	enemy1 = new EnemyShip("enemy2.cfg",
			       2,
			       this->player->ship->look.getX() * gamemapdim.w*3*2
			       -gamemapdim.w*3 +(random() % gamemapdim.w*3*2),
			       this->player->ship->look.getY() * gamemapdim.h*3*2
			       +gamemapdim.h*3 -(random() % gamemapdim.h*3*2)
			       ,  newmove.getX(), newmove.getY(),0.0, 1.0, 1000); // Initialisierung der Gegner
      }
      GlobalListOfSpaceObjects.push_back(enemy1);
    }
  } else {
    int anzahl = level / 4;
    for(int i=1; i<=anzahl * 1;i++){
      newmove.turn(1234); // Zufall???
      enemy1 = new EnemyShip("mini-boss.cfg",
			     23,
			     this->player->ship->look.getX() * gamemapdim.w*3*2
			     -gamemapdim.w*3 +(random() % gamemapdim.w*3*2),
			     this->player->ship->look.getY() * gamemapdim.h*3*2
			     +gamemapdim.h*3 -(random() % gamemapdim.h*3*2)
			     ,  newmove.getX(), newmove.getY(),0.0, 1.0, 1000); // Initialisierung der Gegner
      GlobalListOfSpaceObjects.push_back(enemy1);
    }
  }
  myPause = false;
    GlobalListOfPassiveObjects.push_back(cursor);
    endtime = 0;
    done = false;
    clear = false;
    wantleave = false;
    MessageBox::clear();
    output.str("");
    output<<"Level "<<level<<": "<<player->lifes<<" lifes left";
    MessageBox::addMessage(output.str());
    
    showmap = false;

    if(NULL != player->ship){
      player->ship->setAccel(0.0);
      player->ship->useGun(false);
      player->ship->useLauncher(false);
      player->ship->setSpin(0.0);
      player->ship->move.setLength(0);
    }


}



void OpenSpace::init(){
  quit = false;
  Uint32 endtime = 0;
  clear = false;
  wantleave = false;
  showmap = false;
  zoom = 1200;
  mapposition.x = 10;
  mapposition.y = 14;
  this->reset(level);
}

bool OpenSpace::step(){
  bool running = true;
  bool escape = false;
  while(!escape){
    done = false; // Abbruchbedingung fuer die Hauptschleife
    next_time = SDL_GetTicks() + tickIntervall;
    while(!done){
      while(SDL_PollEvent(&event)){
	switch(event.type){
	case SDL_MOUSEMOTION:
	  mousex = event.motion.x;
	  mousey = -event.motion.y;
	  break;
	case SDL_MOUSEBUTTONDOWN:
	  if(SDL_PRESSED == event.button.state ){
	    switch( event.button.button ){
	    case SDL_BUTTON_RIGHT:
	      if(NULL != player->ship)
		player->ship->useLauncher(true);
	      break;
	    case SDL_BUTTON_LEFT:
	      if(NULL != player->ship)
		player->ship->useGun(true);
	      break;
	    }
	  }
	case SDL_MOUSEBUTTONUP:
	  if(SDL_RELEASED == event.button.state ){
	    switch( event.button.button ){
	    case SDL_BUTTON_RIGHT:
	      if(NULL != player->ship)
		player->ship->useLauncher(false);
	      break;
	    case SDL_BUTTON_LEFT:
	      if(NULL != player->ship)
		player->ship->useGun(false);
	      break;
	    default:
	      break;
	    } 
	  }
	case SDL_KEYDOWN:
	  switch( event.key.keysym.sym ){
	  case SDLK_ESCAPE:
	    //player->lifes = -1;
	    //hasenemies = false;
	    //wantleave = true;
	    escape = true;
	    done = true;
	    //running = false;
	    break;
	  case SDLK_p:
	    if(myPause){
	      myPause = false;
	    } else {
	      myPause = true;
	    }
	    break;
	  case SDLK_n:
	    if(nm){
	      nm = false;
	      MessageBox::addMessage("Maus activated");
	    } else {
	      nm = true;
	      MessageBox::addMessage("Maus deactivated");
	    }
	    break;
	  case SDLK_UP:
	    if(NULL != player->ship)
	      player->ship->setAccel(player->ship->MaxAccel);
	    break;
	    /*case SDLK_DOWN:
	    if(NULL != player->ship)
	      player->ship->setAccel(-1.0);
	    break;
	    */
	  case SDLK_LEFT:
	    if(NULL != player->ship)
	      player->ship->setSpin(1);
	    break;
	  case SDLK_RIGHT:
	    if(NULL != player->ship)
	      player->ship->setSpin(-1);
	    break;
	    /*case SDLK_RSHIFT:
	      player[0]->turbo();
	      break;*/
	  case SDLK_SPACE:
	    if(NULL != player->ship)
	      player->ship->setAccel(player->ship->MaxAccel);
	    break;
	  case SDLK_LALT:
	    if(NULL != player->ship)
	      player->ship->useGun(true);
	    break;
	  case SDLK_LCTRL:
	    if(NULL != player->ship)
	      player->ship->useLauncher(true);
	    break;
	  case SDLK_j:
	    wantleave = true;
	    break;
	  case SDLK_f:
	    SDL_WM_ToggleFullScreen(screen);
	    break;
	  case SDLK_m:
	    if(showmap)
	      showmap = false;
	    else
	      showmap = true;
	    break;
	  case SDLK_PLUS:
	    zoom = zoom *8/10;
	    if(1000 >= zoom)
	      zoom = 1000;
	    //cout<<zoom<<endl;
	    break;
	  case SDLK_MINUS:
	    zoom = zoom *10/8;
	    if(10000 <= zoom)
	      zoom = 10000;
	    //cout<<zoom<<endl;
	    break;
	  case SDLK_v:
	    if(NULL != player->ship){
	      player->rank.incRank();
	      //cout<<"rank: "<<player->rank.getRank()<<endl;
	    }
	    break;
	  case SDLK_b:
	    if(NULL != player->ship)
	      player->rank.decRank();
	    break;
	  case SDLK_a:
	    if(NULL != player->ship)
	      player->rank.setLaufbahn(0);
	    break;
	  case SDLK_y:
	    if(NULL != player->ship)
	      player->rank.setLaufbahn(1);
	    break;
	  case SDLK_x:
	    if(NULL != player->ship)
	      player->rank.setLaufbahn(2);
	    break;
	  case SDLK_c:
	    if(NULL != player->ship)
	      player->rank.setLaufbahn(3);
	    break;
	  case SDLK_r:
	    if(NULL != player->ship)
	      player->ship->rockets += 10;
	    break;
	  case SDLK_t:
	    if(NULL != player->ship)
	      player->ship->rockets -= 10;
	    break;
	  case SDLK_g:
	    if(NULL != player->ship)
	      player->ship->guns++;
	    break;
	  case SDLK_h:
	    if(NULL != player->ship){
	      player->ship->guns--;
	    }
	    break;
	  case SDLK_u:
	      endtime = SDL_GetTicks();
	    break;
	  case SDLK_s:
	    if(NULL != player->ship)
	      player->ship->energy += 5000 ;
	    break;
	  case SDLK_d:
	    if(NULL != player->ship){
	      player->ship->energy -= 100;
	    }
	    break;
	  default:
	    break;
	  }
	  break;
	case SDL_KEYUP:
	  switch( event.key.keysym.sym ){
	  case SDLK_UP:
	    if(NULL != player->ship)
	      player->ship->setAccel(0.0);
	    break;
	  case SDLK_DOWN:
	    if(NULL != player->ship)
	      player->ship->setAccel(0.0);
	    break;
	  case SDLK_LEFT:
	    if(NULL != player->ship)
	      player->ship->setSpin(0);
	    break;
	  case SDLK_RIGHT:
	    if(NULL != player->ship)
	      player->ship->setSpin(0);
	    break;
	  case SDLK_SPACE:
	    if(NULL != player->ship)
	      player->ship->setAccel(0.0);
	    break;
	  case SDLK_LALT:
	    if(NULL != player->ship)
	      player->ship->useGun(false);
	    break;
	  case SDLK_LCTRL:
	    if(NULL != player->ship)
	      player->ship->useLauncher(false);
	    break;
	  case SDLK_j:
	    wantleave = false;
	    break;
	  default:
	    break;
	  }
	  break;
	case SDL_QUIT:
	  player->lifes = -1;
	  hasenemies = false;
	  wantleave = true;
	  done = true;
	  running = false;
	  escape = true;
	  break;
	default:
	  break;
	}
      }

      //double r, m;
      if(!myPause){
	objectcount = 0;
	// Bewegen und ggf Loeschen der Spaceobjekte
	it =  GlobalListOfSpaceObjects.begin();
	while(it !=  GlobalListOfSpaceObjects.end()){
	  objectcount++;
	  // Gravitation...
	  /*if((*it)->pos.getLength() > 1) {
	    m = (*it)->mass;
	    r = ((*it)->pos.getLength()*(*it)->pos.getLength());
	    (*it)->move -= (*it)->pos *( m / (r*r*0.1));
	    }*/
	  if(!(*it)->makeMove()){
	    if(NULL != player->ship && (*it) == player->ship){
	      itp =  GlobalListOfPlayerShips.begin();
	      while(itp !=  GlobalListOfPlayerShips.end()){
		if(*itp == player->ship){
		  player->ship = NULL;
		  GlobalListOfPlayerShips.erase(itp);
		  break;
		}
		itp++;
	      }
	    }
	    delete (*it);
	    it = GlobalListOfSpaceObjects.erase(it);
	  } else {
	    if(it != GlobalListOfSpaceObjects.end()){
	      it++;
	    } else {
	      break;
	    }
	  }
	  //cout<<"b-";
	}

      // Maussteuerung:
	if (!nm) {
	  if(NULL != player->ship) {
	    cursor->setX(mousex*540/640+player->ship->getX() - viewport_m.w/2);
	    cursor->setY(mousey-(-player->ship->getY() - viewport_m.h/2));
	    player->ship->aim(*cursor);
	  }
	} else if(NULL != player->ship) {
	  cursor->setX(player->ship->getX()+player->ship->look.getX()*200);
	  cursor->setY(player->ship->getY()+player->ship->look.getY()*200);
	}
	// Zielerfassung:
	if(NULL != player->ship){
	  player->ship->setTarget(cursor->nextTo(200.0));
	  if(NULL == player->ship->target)
	    player->ship->selectNearestTarget();
	}
	
	

	if(NULL != player->ship) {
	  position_x = (player->ship->getX())/gamemapdim.w;// -1;
	  if(0 > player->ship->getX()) position_x--;
	  position_y = -(player->ship->getY())/gamemapdim.h;// -1;
	  if(0 < player->ship->getY()) position_y--;
	  //position_x = -(int)((player->ship->getX()*2-gamemapdim.w)/gamemapdim.w)*gamemapdim.w;// -1;
	  //position_y = +(int)((player->ship->getY()*2+gamemapdim.h)/gamemapdim.h)*gamemapdim.h;// -1;
	  // Setzen des SpielFlaechenausschnittes

	  viewport_nw.x = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w + gamemapdim.w);
	  viewport_nw.y = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h + gamemapdim.h);
	  viewport_n.x  = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w);
	  viewport_n.y  = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h + gamemapdim.h);
	  viewport_no.x = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w - gamemapdim.w);
	  viewport_no.y = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h + gamemapdim.h);
	  viewport_w.x  = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w + gamemapdim.w);
	  viewport_w.y  = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h);
	  viewport_m.x  = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w);
	  viewport_m.y  = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h);
	  viewport_o.x  = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w - gamemapdim.w);
	  viewport_o.y  = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h);
	  viewport_sw.x = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w + gamemapdim.w);
	  viewport_sw.y = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h - gamemapdim.h);
	  viewport_s.x  = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w);
	  viewport_s.y  = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h - gamemapdim.h);
	  viewport_so.x = (player->ship->getX()  - viewport_m.w/2 - position_x*gamemapdim.w - gamemapdim.w);
	  viewport_so.y = (-player->ship->getY() - viewport_m.h/2 - position_y*gamemapdim.h - gamemapdim.h);
	  
	  //position_x = (player->ship->getX()*2-gamemapdim.w)/gamemapdim.w;// -1;
	  //position_y = -(player->ship->getY()*2+gamemapdim.h)/gamemapdim.h;// -1;
	  
	  //if(0 > viewport_m.x) {mainscreen.x -= viewport_m.x; viewport_m.x = 0;}
	  //if(0 > viewport_m.y) {mainscreen.y -= viewport_m.y; viewport_m.y = 0;}
	  /*
	  if(gamemapdim.w - viewport.w < viewport.x) {viewport.x = gamemapdim.w - viewport.w;}
	  if(gamemapdim.h - viewport.h < viewport.y) {viewport.y = gamemapdim.h - viewport.h;}
	  */

	  
	  /*	  bgview1.x = (player->ship->getX() - viewport_m.w/2) * (bg1->w - viewport_m.w* bg1->w/gamemapdim.w)/gamemapdim.w;
	  bgview1.y = (-player->ship->getY() - viewport_m.h/2) * (bg1->h - viewport_m.h* bg1->h/gamemapdim.h)/gamemapdim.h;
	  */
	  /*
	  if(0 > bgview1.x) {bgview1.x = 0;}
	  if(0 > bgview1.y) {bgview1.y = 0;}
	  / *
	  if((gamemapdim.w - viewport.w)* (bg1->w - viewport.w* bg1->w/background->w)/background->w < bgview1.x) {bgview1.x = (gamemapdim.w - viewport.w)* (bg1->w - viewport.w* bg1->w/background->w)/background->w;}
	  if((gamemapdim.h - viewport.h) * (bg1->h - viewport.h* bg1->h/background->h)/background->h < bgview1.y) {bgview1.y = (gamemapdim.h - viewport.h) * (bg1->h - viewport.h* bg1->h/background->h)/background->h;}
	  */

	  /*
	  bgview2.x = (player->ship->getX() - viewport_m.w/2) * (bg2->w - viewport_m.w* bg2->w/gamemapdim.w)/gamemapdim.w;
	  bgview2.y = (-player->ship->getY() - viewport_m.h/2) * (bg2->h - viewport_m.h* bg2->h/gamemapdim.h)/gamemapdim.h;
	  */
	  /*
	  if(0 > bgview2.x) {bgview2.x = 0;}
	  if(0 > bgview2.y) {bgview2.y = 0;}
	  / *
	  if((gamemapdim.w - viewport.w)* (bg2->w - viewport.w* bg2->w/background->w)/background->w < bgview2.x) {bgview2.x = (gamemapdim.w - viewport.w)* (bg2->w - viewport.w* bg2->w/background->w)/background->w;}
	  if((gamemapdim.h - viewport.h) * (bg2->h - viewport.h* bg2->h/background->h)/background->h < bgview2.y) {bgview2.y = (gamemapdim.h - viewport.h) * (bg2->h - viewport.h* bg2->h/background->h)/background->h;}
	  */

	  /*
	  bgview3.x = (player->ship->getX() - viewport_m.w/2) * (bg3->w - viewport_m.w* (bg3->w)/gamemapdim.w)/gamemapdim.w;
	  bgview3.y = (-player->ship->getY() - viewport_m.h/2) * (bg3->h - viewport_m.h* (bg3->h)/gamemapdim.h)/gamemapdim.h;
	  */
	  /*
	  if(0 > bgview3.x) {bgview3.x = 0;}
	  if(0 > bgview3.y) {bgview3.y = 0;}
	  / *
	  if((gamemapdim.w - viewport.w)* (bg3->w - viewport.w* bg3->w/background->w)/background->w < bgview3.x) {bgview3.x = (gamemapdim.w - viewport.w)* (bg3->w - viewport.w* bg3->w/background->w)/background->w;}
	  if((gamemapdim.h - viewport.h) * (bg3->h - viewport.h* bg3->h/background->h)/background->h < bgview3.y) {bgview3.y = (gamemapdim.h - viewport.h) * (bg3->h - viewport.h* bg3->h/background->h)/background->h;}
	  */
	  //bgview4.x = (player->getX() - viewport.w/2) * (bg4->w - viewport.w* bg4->w/background->w)/background->w;
	  //bgview4.y = (-player->getY() - viewport.h/2) * (bg4->h - viewport.h* bg4->h/background->h)/background->h;

	  /*
	  bgview4.x = (player->ship->getX() - viewport.w/2) * (bg4->w - viewport.w)/background->w;
	  bgview4.y = (-player->ship->getY() - viewport.h/2) * (bg4->h - viewport.h)/background->h;
	 
	  / *
	  if(0 > bgview4.x) {bgview4.x = 0;}
	  if(0 > bgview4.y) {bgview4.y = 0;}
	  / *
	  //if((gamemapdim.w - viewport.w)* (bg4->w - viewport.w* bg4->h/background->h)/background->w < bgview4.x) {bgview4.x = (gamemapdim.w - viewport.w)* (bg4->w - viewport.w* bg4->w/background->w)/background->w;}
	  //if((gamemapdim.h - viewport.h) * (bg4->h - viewport.h* bg4->h/background->h)/background->h < bgview4.y) {bgview4.y = (gamemapdim.h - viewport.h) * (bg4->h - viewport.h* bg4->h/background->h)/background->h;}
	  if((gamemapdim.w - viewport.w)* (bg4->w - viewport.w)/background->w < bgview4.x) {
	    bgview4.x = (gamemapdim.w - viewport.w)* (bg4->w - viewport.w)/background->w;
	  }
	  if((gamemapdim.h - viewport.h) * (bg4->h - viewport.h)/background->h < bgview4.y) {bgview4.y = (gamemapdim.h - viewport.h) * (bg4->h - viewport.h)/background->h;}
	  */




	}
	

	SDL_FillRect(minimap, NULL, 0);
	SDL_FillRect(bigmap, NULL, 0);
	
	int heigth = 200;
	int width = heigth;
	int field;
	
	// Kolisionsabfrage:
	// (Erst alle bewegen und dan die Kollisionsabfrage ist besser in zwei getrennten schleifen)
	hasenemies = false;


	CollisionsMap.clear();
	
	it =  GlobalListOfSpaceObjects.begin();	
	while(it !=  GlobalListOfSpaceObjects.end()){
	  field = ((*it)->getX()/width) * 1000 + ((*it)->getY()/heigth);
	  
	  //if(CollisionsMap.find(field) == CollisionsMap.end()){
	  CollisionsMap[field].push_back((*it));
	  //}
	  if(NULL != player->ship){
	    if(showmap)
	      (*it)->indicate(bigmap,
			      player->ship->getX(),
			      player->ship->getY(),
			      zoom*5,zoom*5);//x*gamemapdim.w, y*gamemapdim.h);
	    (*it)->indicate(minimap,
			    player->ship->getX(),
			    player->ship->getY(),
			    zoom,zoom);//x*gamemapdim.w, y*gamemapdim.h);
	  }
	  
	  it++;
	}


	/*
	it =  GlobalListOfSpaceObjects.begin();	
	while(it !=  GlobalListOfSpaceObjects.end()){
	  field = ((*it)->getX()/width) * 1000 + ((*it)->getY()/heigth);
	  
q	  //if(CollisionsMap.find(field) == CollisionsMap.end()){
	  CollisionsMap[field].push_back((*it));
	  //}
	  
	  ity =  it;
	  ity++;
	  while(ity !=  GlobalListOfSpaceObjects.end()){
	    //(*ity)->show(gamemap);
	    if(NULL != player->ship){
	      if(showmap)
		(*it)->indicate(bigmap,
				player->ship->getX(),
				player->ship->getY(),
				zoom*5,zoom*5);//x*gamemapdim.w, y*gamemapdim.h);
	      (*it)->indicate(minimap,
			      player->ship->getX(),
			      player->ship->getY(),
			      zoom,zoom);//x*gamemapdim.w, y*gamemapdim.h);
	    }
	    Distance->setXY((*it)->getPos().getX()-(*ity)->getPos().getX(),(*it)->getPos().getY()-(*ity)->getPos().getY());
	    ++collisionscheckcount;
	    if(Distance->getLength() <= (*it)->getWidth()/2.0 + (*ity)->getWidth()/2.0){
	      (*it)->collision(&**ity);
	    }
	    ity++;
	  }
	  hasenemies = hasenemies | (*it)->isEnemy();
	  it++;
	}
	*/

	mapit =  CollisionsMap.begin();	
	while(mapit !=  CollisionsMap.end()){
	  it =  (*mapit).second.begin();
	  while(it !=  (*mapit).second.end()){
	    ity =  it;
	    ity++;
	    while(ity !=  (*mapit).second.end()){
	      
	      if((*it)->collisionsCheck((*ity)) && (*ity)->collisionsCheck((*it))){
		(*it)->collision(&**ity);
		//(*ity)->collision(&**it);
	      }
	      ity++;
	    }
	    hasenemies = hasenemies | (*it)->isEnemy();
	    it++;
	  }
	  mapit++;
	}
	
	
	
	// Abbruchbedingungen:
	if (0 == endtime && (NULL == player->ship )){
	  player->lifes--;
	  if(0 >= player->lifes){
	    running = false;
	    escape = true;
	    MessageBox::addMessage("GAME OVER");		  
	  } else {
	    
	    MessageBox::addMessage("you lost...");
	  }
	  endtime = SDL_GetTicks() + 2000;
	}
	
	if (0 == endtime && !clear && (!hasenemies)){
	  MessageBox::addMessage("Level cleared!");
	  if (3 >= level){
	    MessageBox::addMessage("just collect all usfull things");
	    MessageBox::addMessage("and proceed with 'J' to the next level");
	  }
	  //	  endtime = SDL_GetTicks() + 2000;
	  clear = true;
	}
	
	if (0 == endtime && (!hasenemies && wantleave)){
	  endtime = SDL_GetTicks();
	}
	
	// Statusanzeigen:
	SDL_FillRect(status, NULL, 0);
	if(NULL != player->ship && NULL != player->ship->target) {
	  target.x = player->ship->target->getX() - 50;
	  target.y = -player->ship->target->getY() - 60;
	  player->ship->target->showenergy(10, 150, 80, 10, status);
	  //player->ship->target->showint(10, 110, 80, 8, player->ship->target->energy, status);
	  player->ship->target->showshield(10, 124, 80, 10, status);
	}
	if(NULL != player->ship){
	  player->ship->showenergy(10, 378, 80, 12, status);
	  player->ship->showint(10, 378, 80, 12, player->ship->energy/100 ,status);
	  //player->ship->showint(10, 462, 80, 8, player->ship->energy, status);
	  if(player->ship->score < 0) player->ship->score = 0;
	  player->rank.show(status, 10, 294);
	  player->ship->showbar(50,294,40,12, player->score - player->lastScoreGotHigh, player->nextScoreGetHigh - player->lastScoreGotHigh, status);
	  //player->ship->showint(50,298,40,12, player->nextScoreGetHigh, status);
	  player->ship->showint(50, 322, 40, 12, player->ship->score, status);	  
	  player->ship->showshield(10, 349, 80, 12, status);
	  player->ship->showint(10, 349, 80, 12, player->ship->shield/100 ,status);
	  player->ship->showrockets(50, 406, 40, 12, status);
	  player->ship->showint(50, 406, 40, 12, player->ship->rockets, status);
	  player->ship->showguns(10, 406, 30, 12, status);
	  //player->ship->showint(10, 406, 30, 12, player->ship->guns, status);
	  player->ship->showint(10, 434, 30, 12, player->lifes, status);
	  player->ship->showint(50, 434, 40, 12, level, status);
	  //player->ship->showint(50, 406, 40, 12, player->ship->look.getAngle()*100, status);
	}
	
	SDL_FillRect(screen, NULL, 0);
	//SDL_BlitSurface(bg4, &bgview4, screen, &mainscreen);
	//SDL_BlitSurface(bg3, &bgview3, screen, &mainscreen);
	//SDL_BlitSurface(bg2, &bgview2, screen, &mainscreen);
	//SDL_BlitSurface(bg1, &bgview1, screen, &mainscreen);
	
	if(showmap){
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(bigmap, NULL, screen, &mainscreen);      
	} else {
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x -1,position_y -1), &viewport_nw, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x,position_y -1), &viewport_n, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x +1,position_y -1), &viewport_no, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x -1,position_y), &viewport_w, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x,position_y), &viewport_m, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x +1,position_y), &viewport_o, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x -1,position_y +1), &viewport_sw, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x,position_y +1), &viewport_s, screen, &mainscreen);
	  mainscreen.x = 100;
	  mainscreen.y = 0;
	  SDL_BlitSurface(sector.draw(position_x +1,position_y +1), &viewport_so, screen, &mainscreen);
	}
	  /*
	    map<int,map<int,SpaceSector> >::iterator it;
	    it = sectors.find(0);
	    if (it != sectors.end()) {
	    map<int,SpaceSector>::iterator it2;
	    it2 = ((it)->second).find(0);
	    if (it2 != ((it)->second).end()) {
   	    SDL_BlitSurface(((it2)->second).draw(), &viewport_m, screen, &mainscreen);      
	    }
	    }
	  */
	  
	  /*	it = sectors.find(1);
		if (it != sectors.end()) {
		map<int,SpaceSector*>::iterator it2;
<		it2 = ((it)->second).find(0);
		if (it2 != ((it)->second).end()) {
		SDL_BlitSurface(((it2)->second)->draw(), &viewport_m, screen, &mainscreen);      
		}
		}*/
	  //SDL_BlitSurface((spacemap->draw()), &viewport, screen, &mainscreen);      
	SDL_BlitSurface(status, NULL, screen, &statusscreen);
	SDL_BlitSurface(minimap, NULL, screen, &mapposition);      
	SDL_BlitSurface(statusframe, NULL, screen, &statusscreen);      
	if(NULL != player->ship){
	  //player->ship->showint(10, 10, 620, 15, collisionscheckcount, screen);
	  //player->ship->showint(10, 30, 620, 15, collisionscheckcount2, screen);
	  //player->ship->showint(10, 50, 620, 15, objectcount, screen);
	}
	  
	  //list<SpaceObject*>::iterator it;
	  
	
	


	MessageBox::show(screen);
      }
      SDL_Flip(screen);
      SDL_Delay(time_left());
      next_time += tickIntervall;
      
      if(0 != endtime && endtime <= SDL_GetTicks()){
	done = true;
      }
      if(NULL != player->ship){
	player->score = player->ship->score;
	if(false && player->lastScoreGotLive + 10000 <= player->score){
      	  player->lastScoreGotLive += 10000;
	  player->lifes++;
	  output.str("");
	  output<<"Got an extra life ("<<player->lifes<<" lifes)";
	  MessageBox::addMessage(output.str());
	}
	if(player->nextScoreGetHigh <= player->score){
      	  player->lastScoreGotHigh = player->nextScoreGetHigh;
	  player->nextScoreGetHigh += 500 + 300 * player->rank.getRank();
	  player->rank.incRank();
	  player->ship->MaxEnergy = 5000 + 1000 * player->rank.getRank();
	  player->ship->energy = 5000 + 1000 * player->rank.getRank();
	  player->ship->MaxShield = 5000 + 1000 * player->rank.getRank();
	  player->ship->shield = 5000 + 1000 * player->rank.getRank();
	  output.str("");
	  output<<"you have been promoted";
	  MessageBox::addMessage(output.str());
	}
      }
    }  
    player->level = level;
    if (NULL == player->ship){
      this->reset(level);
    } else {
      if(clear && !(escape))
	this->reset(++level);
    }
  }
  return running;
}

void OpenSpace::run(){
  init();
  while(step()){
  }
  //  player->setSpin(0);
  //return player;
}
