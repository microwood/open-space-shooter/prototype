#include <iostream>
#include <math.h>
#include "vector.h"


using namespace std;

int main(int argc, char **argv){
  myVector v1, v2, v3;
  v1.setLength(1);
  v1.setAngle(0);
  v2.setLength(1);
  v2.setAngle(M_PI_2);
  cout<<"v1 = ";  v1.toScreen();
  cout<<"v2 = ";  v2.toScreen();
  v1 += v2;
  cout<<"v1 = ";  v1.toScreen();
  v2 *= 5;
  cout<<"v2 = ";  v2.toScreen();
  v3 = v2.proj(v1);
  cout<<"v3 = ";  v3.toScreen();
  return 0;
}
