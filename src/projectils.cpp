#include "openspace.h"
#include "projectils.h"
#include "vector.h"
#include <iostream>

using namespace std;

Bullet::Bullet(const char* initfile, double px, double py, double mx, double my, double lx, double ly, int m, int dur):
  SpaceObject(initfile,px,py,mx,my,-1.0,0.0){
  MaxSpeed = 200;
  mass = 1;
  damage = 20;
  accel = 0;
  spin = 0;
  MaxEnergy = 1;
  energy = 1;
  expire = dur;
  i = 1;
}

Bullet::~Bullet(){
}

int Bullet::makeMove(){
  this->i++;
  this->accelerate(this->accel);
  this->turn(-2);
  //  this->turn(this->spin);
  pos += move;

  if(this->expire < this->i){
    return 0;
  }

  /*
  if(-100 > pos.getX()) {return 0;}
  if(-100 > -pos.getY()) {return 0;}
  if(gamemapdim.w +100  < pos.getX()) {return 0;}
  if(gamemapdim.h +100 < -pos.getY()) {return 0;}
  */

  destination.x = (short int)pos.getX()-width/2;

  destination.y = -((short int)pos.getY())-heigth/2;
  //cout<<"move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
  if(0 >= energy)
    return 0;
  else
    return 1;
}

Rocket::Rocket(SpaceObject *tar, double px, double py, double mx, double my, double lx, double ly, int m):
  SpaceObject("rocket.cfg", px, py, mx, my, lx, ly){    
  MaxSpeed = 7;
  mass = 1;
  damage = 2000;
  accel = 0;
  spin = 0;
  MaxEnergy = 500;
  energy = 500;
  expire = 50;
  i = -1;
  setTarget(tar);
}
Rocket::~Rocket(){
  //  cout<<endl;
  //  cout<<"Rocked has died ("<<(int)this % 1000<<")"<<endl;
  Detonation* deto;
  deto = new Detonation("detonation.cfg",
		       this->pos.getX(),
		       this->pos.getY(),
		       this->move.getX(),
		       this->move.getY(), 8);
  GlobalListOfSpaceObjects.push_back(deto);
}


void Rocket::aim(SpaceObject &target){
  myVector direction; 
  //direction = (target.pos + target.move * 10) - (this->pos  + this->move * 10);
  //double distance = (target.pos - this->pos).getLength();
  direction = (target.pos + target.move*0.1*target.move.getLength())
            - (this->pos - this->move*0.1*this->move.getLength());
  //direction = target.pos - this->pos;
  double deltaAngle = direction.getAngle() - look.getAngle();
  if(M_PI < abs(deltaAngle) )
    if(0 < deltaAngle)
      this->spin = -1.5;
    else
      this->spin = 1.5;
  else
    if(0 < deltaAngle)
      this->spin = 1.5;
    else
      this->spin = -1.5;
}

bool Rocket::isEnemy(){
  return false;
  if(this->expire > this->i) {
    return true;
  } else {
    return false;
  }
}
 
int Rocket::makeMove(){
  if(0 >= energy)
    return 0;
  else {
    if(0 > i){
      i++;
    } else if(this->expire > this->i) {
      if(NULL == target) {
	i++;
	this->spin = 0.0;
	accelerate(MaxSpeed/2.0);
      } else {
	aim(*target);
	myVector direction; 
	//double distance = (target->pos - this->pos).getLength();
	direction = (target->pos + target->move) - (this->pos - this->move);
	double deltaAngle = direction.getAngle() - look.getAngle();
	if(M_PI/16 > abs(deltaAngle)) {
	  i++;
	  accelerate(MaxSpeed/2.0);
	}
      }
    }
    this->turn(this->spin);

    pos += move;
    
    /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */

    destination.x = (short int)pos.getX()-width/2;
    
    destination.y = -((short int)pos.getY())-heigth/2;
    //cout<<"Rocket: move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
    //cout<<"Rocket-Target: "<<endl;
    //target.look.toScreen();
    return 1;  
  }
}

Torpedo::Torpedo(SpaceObject *tar, double px, double py, double mx, double my, double lx, double ly, int m):
  SpaceObject("torpedo.cfg", px, py, mx, my, lx, ly){    
  MaxSpeed = 3;
  mass = 1;
  damage = 20000;
  accel = 0;
  spin = 0;
  MaxEnergy = 500;
  energy = 500;
  expire = 100;
  i = -1;
  setTarget(tar);
}


Torpedo::~Torpedo(){
  //  cout<<endl;
  //  cout<<"Rocked has died ("<<(int)this % 1000<<")"<<endl;
  Detonation* deto;
  deto = new Detonation("detonation-70.cfg",
		       this->pos.getX(),
		       this->pos.getY(),
		       this->move.getX(),
		       this->move.getY(), 8);
  GlobalListOfSpaceObjects.push_back(deto);
}


void Torpedo::aim(SpaceObject &target){
  myVector direction; 
  //direction = (target.pos + target.move * 10) - (this->pos  + this->move * 10);
  //double distance = (target.pos - this->pos).getLength();
  direction = (target.pos + target.move*0.1*target.move.getLength())
            - (this->pos - this->move*0.1*this->move.getLength());
  //direction = target.pos - this->pos;
  double deltaAngle = direction.getAngle() - look.getAngle();
  if(M_PI < abs(deltaAngle) )
    if(0 < deltaAngle)
      this->spin = -0.5;
    else
      this->spin = 0.5;
  else
    if(0 < deltaAngle)
      this->spin = 0.5;
    else
      this->spin = -0.5;
}

bool Torpedo::isEnemy(){
  return false;
  if(this->expire > this->i) {
    return true;
  } else {
    return false;
  }
}
 
int Torpedo::makeMove(){
  if(0 >= energy)
    return 0;
  else {
    if(0 > i){
      i++;
    } else if(this->expire > this->i) {
      if(NULL == target) {
	i++;
	this->spin = 0.0;
	accelerate(MaxSpeed/2.0);
      } else {
	aim(*target);
	myVector direction; 
	//double distance = (target->pos - this->pos).getLength();
	direction = (target->pos + target->move) - (this->pos - this->move);
	double deltaAngle = direction.getAngle() - look.getAngle();
	if(M_PI/16 > abs(deltaAngle)) {
	  i++;
	  accelerate(MaxSpeed/2.0);
	}
      }
    }
    this->turn(this->spin);

    pos += move;
    
    /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */

    destination.x = (short int)pos.getX()-width/2;
    
    destination.y = -((short int)pos.getY())-heigth/2;
    //cout<<"Rocket: move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
    //cout<<"Rocket-Target: "<<endl;
    //target.look.toScreen();
    return 1;  
  }
}

