#include "menu.h"
#include "graphics.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>



Menu::Menu(SDL_Surface *nscreen){
  this->screen = nscreen;

  if(!TTF_WasInit() && -1 == TTF_Init()) {
    printf("TTF_Init: %s\n", TTF_GetError());
    exit(1);
  }
  char* font_file = "data/Neuropol.ttf";
  size = 16;
  if( (font = TTF_OpenFont(font_file, size)) == NULL) {
    printf("TTF_OpenFont error: %s", TTF_GetError());
    exit (-1);
  }

  text_color.r = 0xff;
  text_color.g = 0xff;
  text_color.b = 0xff;

  title = "";
  for(int i = 0;i<10;++i)
    options[i] = "";
  

}


int Menu::run(){

  int returnvalue = 0;
  int zeilen = 4;

  for(int i=1;i<10;++i){
    if ("" != options[i])
      ++zeilen;
  }
  if ("" != options[0])
    ++zeilen;



  dest.x = 100 - size;
  dest.y = 100 - size;
  dest.w = DISPLAY_W - 200 + 2*size;
  dest.h = zeilen * size + 2*size;

  SDL_FillRect(screen, &dest, 900);

  dest.x = 100;
  dest.y = 100;

  
  printline(title);
  printline("");
  printline("");
  for(int i=1;i<10;++i){
    if ("" != options[i])
      printline(options[i]);
  }
  printline("");
  if ("" != options[0])
    printline(options[0]);

  bool done = false;
  SDL_Event event;

  while(!done){
    while(SDL_PollEvent(&event)){
      switch(event.type){
      case SDL_KEYDOWN:
	switch( event.key.keysym.sym ){
	case SDLK_UP:
	  
	  break;
	case SDLK_DOWN:
	  
	  break;
	case SDLK_LEFT:

	  break;
	case SDLK_RIGHT:
	  break;
	case SDLK_SPACE:

	  break;
	case SDLK_0 :
	  done = true;
	  returnvalue = 0;
	  break;
	case SDLK_1 :
	  done = true;
	  returnvalue = 1;
	  break;
	case SDLK_2 :
	  done = true;
	  returnvalue = 2;
	  break;
	case SDLK_3 :
	  done = true;
	  returnvalue = 3;
	  break;
	case SDLK_4 :
	  done = true;
	  returnvalue = 4;
	  break;
	case SDLK_5 :
	  done = true;
	  returnvalue = 5;
	  break;
	case SDLK_6 :
	  done = true;
	  returnvalue = 6;
	  break;
	case SDLK_7 :
	  done = true;
	  returnvalue = 7;
	  break;
	case SDLK_8 :
	  done = true;
	  returnvalue = 8;
	  break;
	case SDLK_9 :
	  done = true;
	  returnvalue = 9;
	  break;

	default:
	  break;
	}
	break;
      case SDL_QUIT:
	  done = true;
	  returnvalue = 0;	
	break;
      default:
	break;
      }
    }
    SDL_Flip(screen);
  }
  
  return returnvalue;
}


void Menu::printline(char* text){
  SDL_Surface *text_surf;
  text_surf = TTF_RenderText_Blended(font, text, text_color);
  SDL_BlitSurface(text_surf, NULL, screen, &dest);      
  dest.y += size;
  SDL_FreeSurface(text_surf);

}


Menu::~Menu() {
  TTF_CloseFont(font);
}
