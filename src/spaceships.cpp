#include "openspace.h"
#include "spaceships.h"
#include "projectils.h"
#include "vector.h"
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;


SpaceShip::SpaceShip(const char* initfile, double px, double py, double mx, double my, double lx, double ly, int m):
  SpaceObject(initfile, px, py, mx, my, lx, ly){  
  gunactive = false;
  launcheractive = false;
  target = NULL;

  nextshot = 0;
  nextrock = 0;
  nextslot = 0;

  mass = (width * heigth)/2 * 16;


  //default Werte:
  MaxAccel = 1.0;
  MaxSpeed = 10;
  guns = 0;
  MaxGuns = 0;
  rockets = 0;
  MaxRockets = 0;
  shieldRegen = 0;
  energy = 10;
  MaxEnergy = 10;
  shield = 0;
  MaxShield = 0;
  agility = 0;
  
  char buffer[100];
  string cfg_line;
  string key;
  string value;
  string initpath = "data/spaceobjects/";
  initfile = initpath.append(initfile).c_str();
  //cout<<initpath<<endl;
  ifstream file (initfile, ios::in);
  while (! file.eof() ) {
    file.getline (buffer,100);
    cfg_line = string(buffer);
    key = cfg_line.substr(0,cfg_line.find("=",0));
    value = cfg_line.substr(cfg_line.find("=",0)+1);
    
    if (0 == key.compare("agility")){
      agility = atof(value.c_str());
    }
    if (0 == key.compare("MaxAccel")){
      MaxAccel = atof(value.c_str());
    }
    if (0 == key.compare("MaxSpeed")){
      MaxSpeed = atoi(value.c_str());
    }
    if (0 == key.compare("guns")){
      guns = atoi(value.c_str());
    }
    if (0 == key.compare("MaxGuns")){
      MaxGuns = atoi(value.c_str());
    }
    if (0 == key.compare("rockets")){
      rockets = atoi(value.c_str());
    }
    if (0 == key.compare("MaxRockets")){
      MaxRockets = atoi(value.c_str());
    }
    if (0 == key.compare("shieldRegen")){
      shieldRegen = atoi(value.c_str());
    }
    if (0 == key.compare("energy")){
      energy = atoi(value.c_str());
    }
    if (0 == key.compare("MaxEnergy")){
      MaxEnergy = atoi(value.c_str());
    }
    if (0 == key.compare("shield")){
      shield = atoi(value.c_str());
    }
    if (0 == key.compare("MaxShield")){
      MaxShield = atoi(value.c_str());
    }
    //if (another key)
  }

}

SpaceShip::~SpaceShip(){
    list<SpaceShip*>::iterator it;
    it =  modules.begin();
    while(it !=  modules.end()){
      delete(*it);
      it++;
    }
  //  cout<<endl;
  //  cout<<"SpaceShip has died ("<<(int)this % 1000<<")"<<endl;
  /* saepter mal, wenn ich scripte verstanden habe...
   * wrakteil;
  char buffer[100];
  int anzahl = 0;
  myVector newpos;
  myVector newspeed;
  string newcfg;
  newpos.setLength(10);
  newspeed.setLength(0.5);
  anzahl = width*heigth;
***
  if (3 == instanz){
    Powerup* pup;
    pup = new Powerup("pwrup_ore.cfg",
		      this->getX(),
		      this->getY(),
		      this->move.getX(),
		      this->move.getY(),
		      8);
    GlobalListOfSpaceObjects.push_back(pup);
  }
  if(0 < anzahl){
    asteo = new Asteorid(newcfg.c_str(),
			 this->getX(),
			 this->getY(),
			 this->move.getX(),
			 this->move.getY(),
			 -1.0,0.0, instanz-1);
    GlobalListOfSpaceObjects.push_back(asteo);
  }
  for(int i=0; i<anzahl;i++){
    newpos.turn(2*M_PI/anzahl);
    newspeed.turn(2*M_PI/anzahl);
    asteo = new Asteorid(newcfg.c_str(),
			 this->getX()+newpos.getX(),
			 this->getY()+newpos.getY(),
			 this->move.getX()+newspeed.getX(),
			 this->move.getY()+newspeed.getY(),
			 -1.0,0.0, instanz-1);
    GlobalListOfSpaceObjects.push_back(asteo);
  }
  */
}


void SpaceShip::useGun(bool status){
  //if(status) fire();
  gunactive = status;
}

void SpaceShip::useLauncher(bool status){
  //if(status) fire();
  launcheractive = status;
}

void SpaceShip::show(SDL_Surface *screen, int x, int y){
  SpaceObject::show(screen, x, y);
  // SpaceShip Module Anzeigen...
  list<SpaceShip*>::iterator it;
  it =  modules.begin();
  while(it !=  modules.end()){
    (*it)->show(screen, x, y);
    it++;
  }
  if(this->isPlayer() || this->isEnemy()){
    showenergy((int)pos.getX()-width/2-x, -heigth/100-3-heigth/2-(int)pos.getY()-y, width, heigth/100+2, screen);
    showshield((int)pos.getX()-width/2-x, -heigth/100-3-heigth/2-(int)pos.getY()-y-heigth/100-3, width, heigth/100+2, screen);  
  }
}


void SpaceShip::fire(SpaceObject *par){
  if(nextshot <= lifetime){
    nextshot = lifetime + 3;
    myVector newdir, newpos, mod = look;
    int lguns = guns; //(int)sqrt((double)(guns-1))+1;
    double spread = (M_PI/2.0)*lguns/5.0;
    if(spread > M_PI/2.0) spread = M_PI/2.0;
    int distance = 2; // mindestens 5
    int speed = 50; // sollte kleinergleich 100 sein
    newdir.setXY(look.getX(),look.getY());
    //newdir.turn(-M_PI/48);
    //for(int i=0; i<240*2;i++){
    MetaSpaceObject *metaShoot;
    Bullet *bullet;
    if(1 < lguns)
      mod.turn(-spread/2.0);
    for(int j=0; j<lguns; j++){
      metaShoot = new MetaSpaceObject();
      // metaShoot-> = true;
      for(int i=speed/distance; i>0;i--) {
	newpos.setXY(getX()+mod.getX()*(width*1.2)-look.getX()*distance*i-move.getX(),
		     getY()+mod.getY()*(heigth*1.2)-look.getY()*distance*i-move.getY());
	bullet = new Bullet("bullet.cfg",
			    newpos.getX(),
			    newpos.getY(),
			    newdir.getX()*speed+move.getX(),
			    newdir.getY()*speed+move.getY());
	bullet->parent = par;
	metaShoot->addObject(bullet);
      }
      if(1 < lguns)
	mod.turn(spread/(lguns-1));
      GlobalListOfSpaceObjects.push_back(metaShoot);
    }
    //newdir.turn(M_PI/240);
    //look.turn(M_PI/240);
    //}
  }
}

void SpaceShip::fireRocket(SpaceObject *par, SpaceObject *target){
  if(0 < rockets && nextrock <= lifetime){
    rockets--;
    nextrock = lifetime + 10;
    myVector newdir, newpos, mod = look;
    double spread = M_PI;
    //int guns = 2;
    //guns--;
    newdir.setXY(look.getX(),look.getY());
    //newdir.turn(-M_PI/48);
    //for(int i=0; i<240*2;i++){
    Rocket *rocket;
    if (0 == nextslot) {
      mod.turn(-spread/2.0);
      nextslot = 1;
    } else {
      mod.turn(spread/2.0);
      nextslot = 0;
    }
    //for(int j=0; j<=guns; j++){
      newpos.setXY(getX()+mod.getX()*(width)+look.getX(),
		   getY()+mod.getY()*(heigth)+look.getY());
      rocket = new Rocket(target,
				  newpos.getX(),
				  newpos.getY(),
				  newdir.getX()*5+move.getX(),
				  newdir.getY()*5+move.getY(),
				  newdir.getX(),
				  newdir.getY());
      rocket->parent = par;
      GlobalListOfSpaceObjects.push_back(rocket);
      //mod.turn(spread/guns);
      //}
      //      cout<<endl;
      //      cout<<"Rocket Fired ("<<(int)rocket % 1000<<")"<<endl;
      //      cout<<"Target is: "<<(int)target % 1000<<endl;
  }
  //newdir.turn(M_PI/240);
  //look.turn(M_PI/240);
  //}
}

bool SpaceShip::takeDamage(int damage){
  this->shield -= damage;
  if(0 > this->shield){
    this->energy += this->shield;
    this->shield = 0;
    if(0 > this->energy){
      this->energy = -1;
      return true;
    }
  }
  return false;
}
           


int SpaceShip::makeMove(){
  if(0 == SpaceObject::makeMove())
    return 0;
  else {
  /*  lifetime++;
  if(0 >= energy)
    return 0;
  else {
    this->accelerate(this->accel);
    this->turn(this->spin*agility);
    pos += move;

    destination.x = (short int)pos.getX()-width/2;
    
    destination.y = -((short int)pos.getY())-heigth/2;
    //cout<<"move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
    return 1;
  */

    if(gunactive) {this->fire(this);}
    if(launcheractive) {this->fireRocket(this,target);}

    //    move *= 0.5;
    list<SpaceShip*>::iterator it;
    it =  modules.begin();
    while(it !=  modules.end()){
      (*it)->move = move;
      (*it)->makeMove();
      it++;
    }
    
    shield += shieldRegen;
    if(MaxShield < shield){
      shield = MaxShield;
    }

    /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */
    
    
    return 1;
  }
}

void SpaceShip::showshield(int px, int py, int wi, int he, SDL_Surface *screen){
  showbar(px, py, wi, he, shield, MaxShield, screen);
}


PlayerShip::PlayerShip(const char* initfile, double px, double py, double mx, double my, double lx, double ly, int m, int ki):
  SpaceShip(initfile, px, py, mx, my, lx, ly, m), targetIndicator(new SpaceObject("target.cfg")), target_dir(new SpaceObject("target-dir.cfg"))
{
  this->ki = ki;
  targetlock = false;
  //targetIndicator = new SpaceObject("target.cfg");
  //target_dir = new SpaceObject("target-dir.cfg");
  GlobalListOfPassiveObjects.clear();
  GlobalListOfPassiveObjects.push_back(targetIndicator);
  GlobalListOfPassiveObjects.push_back(target_dir);

  //  cout<<"player initialisized ("<<(int)this % 1000<<")"<<endl;
  r = b = 0;
}

PlayerShip::~PlayerShip(){
  Detonation* deto;
  deto = new Detonation("detonation.cfg",
		       this->pos.getX(),
		       this->pos.getY(),
		       this->move.getX(),
		       this->move.getY(), 8);
  GlobalListOfSpaceObjects.push_back(deto);
  this->targetIndicator->hide();
  this->target_dir->hide();
}

void PlayerShip::show(SDL_Surface *screen, int x, int y){
  SpaceShip::show(screen, x, y);
  //showbar(getX()-heigth, getY(), width, 2, energy, MAXENERGY, screen);
  this->targetIndicator->hide();
  this->target_dir->hide();
  if (NULL != this->target){
    this->targetIndicator->unHide();
    myVector *Distance = new myVector;
    Distance->setXY(-this->getPos().getX()+this->target->getPos().getX(),
		    -this->getPos().getY()+this->target->getPos().getY());
    this->targetIndicator->setPos(this->target->pos);
    if(DISPLAY_H/2 < abs(Distance->getY()) || DISPLAY_W/2 < abs(Distance->getX())){
      this->target_dir->unHide();
      this->target_dir->look.setAngle(Distance->getAngle());
      this->target_dir->turn(0.0);
      this->target_dir->setPos(this->pos);
    }
  delete Distance;
  }
}




void PlayerShip::aim(SpaceObject &target){
  myVector direction;
  //double distance;
  direction = (target.pos) - (this->pos);
  double deltaAngle = direction.getAngle() - look.getAngle();
  //cout<<deltaAngle<<endl;
  if((M_PI/32 > abs(deltaAngle))){
    if(M_PI < abs(deltaAngle))
      if(0 < deltaAngle)
	this->spin = -abs(deltaAngle)*M_PI;
      else
	this->spin = abs(deltaAngle)*M_PI;
    else
      if(0 < deltaAngle)
	this->spin = abs(deltaAngle)*M_PI;
      else
	this->spin = -abs(deltaAngle)*M_PI;
    
  } else {
    if(M_PI*2 - M_PI/32 < abs(deltaAngle)){
      if(M_PI < abs(deltaAngle))
	if(0 < deltaAngle)
	  this->spin = -(M_PI*2-abs(deltaAngle))*M_PI;
	else
	  this->spin = (M_PI*2-abs(deltaAngle))*M_PI;
      else
	if(0 < deltaAngle)
	  this->spin = (M_PI*2-abs(deltaAngle))*M_PI;
	else
	  this->spin = -(M_PI*2-abs(deltaAngle))*M_PI;
      
    } else if(M_PI < abs(deltaAngle))
      if(0 < deltaAngle)
	this->spin = -1.0;
      else
	this->spin = 1.0;
    else
      if(0 < deltaAngle)
	this->spin = 1.0;
      else
	this->spin = -1.0;
  }
  //cout<<this->spin<<endl;
}


void PlayerShip::selectNextTarget(){
    list<SpaceObject*>::iterator it;
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it) == this->target)
	break;
      it++;
    }
    it++;
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it)->isTarget()){
	this->target = (*it);
	return;
      }
      it++;
    }
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it)->isTarget()){
	this->target = (*it);
	return;
      }
      it++;
    }
}


void PlayerShip::selectNextEnemyTarget(){
    list<SpaceObject*>::iterator it;
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it) == this->target)
	break;
      it++;
    }
    it++;
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it)->isEnemyShip()){
	this->target = (*it);
	return;
      }
      it++;
    }
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it)->isEnemyShip()){
	this->target = (*it);
	return;
      }
      it++;
    }    
}

void PlayerShip::selectNearestTarget(){
    double distance = 10.0;
    myVector direction; 
    
    list<SpaceObject*>::iterator it;
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      direction = ((*it)->pos + (*it)->move) - (this->pos - this->move);
      if((*it)->isTarget() && distance > ((*it)->pos - this->pos).getLength()){
	this->target = (*it);
	distance = ((*it)->pos - this->pos).getLength();
      }
      it++;
    }
}

void PlayerShip::targetLock(){
  targetlock = true;
  targetIndicator->turn(5.0);
}

void PlayerShip::targetUnlock(){
  targetlock = false;
  targetIndicator->turn(-5.0);
}

bool PlayerShip::targetIsLocked(){
  return targetlock;
}

void PlayerShip::setTarget(SpaceObject* target){
  if(NULL != target && !this->targetlock){
    this->target = target;
    this->target->isTrackedBy(this);
  }
}

void PlayerShip::showrockets(int px, int py, int wi, int he, SDL_Surface *screen){
  showbar(px, py, wi, he, rockets, MaxRockets, screen, 255,255,255,126,126,255,0,0,255);
}

void PlayerShip::showguns(int px, int py, int wi, int he, SDL_Surface *screen){
  //  showbar(px, py, wi, he, guns, MaxGuns, screen, 255,255,255,255,126,126,255,0,0);
  showbar(px, py, wi, he, guns, MaxGuns, screen, 255,0,0,255,0,0,255,0,0);
}



int PlayerShip::makeMove(){
  if(0 == SpaceObject::makeMove())
    return 0;
  else {
  /*  lifetime++;
  if(0 >= energy){
    return 0;
    //energy = MaxEnergy;
    //return 1;
  
  } else {

  */
    if(ki){
      double distance = 5000.0;
      list<SpaceObject*>::iterator it;
      SpaceObject* newTarget = NULL;
      it =  GlobalListOfSpaceObjects.begin();
      while(it !=  GlobalListOfSpaceObjects.end()){
	if( (*it)->isEnemy() && (distance > ((*it)->pos - this->pos).getLength())){
	  newTarget = (*it);
	  distance = ((*it)->pos - this->pos).getLength();
	}
	it++;
      }
      setTarget(newTarget);
      
      myVector direction; 
      double deltaAngle;
      if(target){
	this->aim(*target);
	
	this->accelerate(this->accel);  
	direction = target->getPos() - this->getPos();
	deltaAngle = direction.getAngle() - this->getLook().getAngle();
	if(abs(deltaAngle) < M_PI/(48)){
	  this->fire(this);
	}    
	if(nextrock <= lifetime){
	  this->fireRocket(this,target);
	  nextrock += 40;
	}
	
      } else {
	this->spin = 0.0;
      }
    }

    Uint32 now;
    now = SDL_GetTicks();
    //if(turbotime + 50 <= now){
//    this->accelerate(this->accel);
    //}
//    this->turn(this->spin*agility);
    if(gunactive) {this->fire(this);}
    if(launcheractive) {this->fireRocket(this,target);}
//    pos += move;
    // bremse...
    //move *= 0.95;

    //if(NULL == this->target){
    //  selectNearestTarget();
    //}
    
    shield += shieldRegen;
    if(MaxShield < shield){
      shield = MaxShield;
    }

    /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */
    
    
//    destination.x = (short int)pos.getX()-width/2;
    
//    destination.y = -((short int)pos.getY())-heigth/2;
    //cout<<"move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
    return 1;
  }
}

EnemyShip::EnemyShip(const char* initfile, int t, double px, double py, double mx, double my, double lx, double ly, int m ):
  SpaceShip(initfile, px, py, mx, my, lx, ly, m ) {
  this->accel = MaxAccel; // Bleifuss
  g = b = 0;
  //  cout<<"enemy initialisized ("<<(int)this % 1000<<")"<<endl;
}

EnemyShip::~EnemyShip(){
    Detonation* deto;
    deto = new Detonation("detonation-70.cfg",
			  this->pos.getX(),
			  this->pos.getY(),
			  this->move.getX(),
			  this->move.getY(), 8);
    GlobalListOfSpaceObjects.push_back(deto);
  Powerup* pup;
  myVector newpos;
  myVector newspeed;
  myVector newdir;
  newpos.setLength(1);
  newspeed.setLength(1);
  newdir.setLength(1);
  int anzahl = 3;
  for(int i=0; i<anzahl;i++){
    int i = random() % 10;
    string newcfg;
    newpos.turn(2*M_PI/anzahl);
    newdir.turn(2*M_PI/anzahl);
    if(0 <= i && i <= 1)
      newcfg = "pwrup_cache.cfg";
    if(2 <= i && i <= 3)
      newcfg = "pwrup_shield.cfg";
    if(4 <= i && i <= 5)
      newcfg = "pwrup_rockets.cfg";
    if(6 <= i && i <= 7)
      newcfg = "pwrup_guns.cfg";
    newspeed.setX(this->move.getX());
    newspeed.setY(this->move.getY());
    if(1 < newspeed.getLength())
      newspeed.setLength(1);
    if(!(8 <= i && i <= 9)) {
      pup = new Powerup(newcfg.c_str(),
			this->getX()+newpos.getX(),
			this->getY()+newpos.getY(),
			newspeed.getX()+newdir.getX(),
			newspeed.getY()+newdir.getY(),
			i);
      GlobalListOfSpaceObjects.push_back(pup);
    }
  }
  
}

void EnemyShip::antiaim(SpaceObject &target){
  myVector direction;
  double distance;
  distance = (target.pos - this->pos).getLength();
  direction = (this->pos + this->move * 10) - (target.pos  + target.move * 10);
  double deltaAngle = direction.getAngle() - look.getAngle();
  if(M_PI/32 > abs(deltaAngle)){
    this->spin = 0.0;
  } else if(M_PI < abs(deltaAngle))
    if(0 < deltaAngle)
      this->spin = -1.0;
    else
      this->spin = 1.0;
  else
    if(0 < deltaAngle)
      this->spin = 1.0;
    else
      this->spin = -1.0;
}

int EnemyShip::makeMove(){
  if(0 == SpaceObject::makeMove())
    return 0;
  // lifetime++;  
  //if(0 >= energy)
  //return 0;
  else {
    double distance = 10000.0;
    //double distance = 000.0;
    
    list<SpaceObject*>::iterator it;
    SpaceObject* newTarget = NULL;
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it)->isPlayer() && distance > ((*it)->pos - this->pos).getLength()){
	newTarget = (*it);
	distance = ((*it)->pos - this->pos).getLength();
      }
      it++;
    }
    setTarget(newTarget);
    
    myVector direction;
    myVector folowHim;
    double deltaAngle;
    if(NULL != target){
      if(150 > distance){
	folowHim = target->move;
	folowHim *= -1;             
	this->aim(folowHim);
	/*
	direction = this->getPos() - target->getPos();
	if(direction.getAngle() < target->move.getAngle())
	  direction.turn(M_PI/2);
	else
	  direction.turn(-M_PI/2);
	this->aim(direction);
	*/
      } else {
	this->aim(*target);
      }
      //this->accelerate(this->accel);  
      direction = target->getPos() - this->getPos();
      deltaAngle = direction.getAngle() - this->getLook().getAngle();
      if(abs(deltaAngle) < M_PI/(24) && distance < 300 ){
	this->fire(this);
      }    
      if(nextrock <= lifetime && distance < 500 ){
	this->fireRocket(this,target);
	nextrock = lifetime + 40;
      }
      
    } else {
      this->spin = 0.0;
    }
    
    //this->accelerate(this->accel);
    //    this->turn(this->spin*agility);
    //if(gunactive) {this->fire();}
    //pos += move;
    
    shield += shieldRegen;
    if(MaxShield < shield){
      shield = MaxShield;
    }
    
    /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */
    
    //destination.x = (short int)pos.getX()-width/2;
    
    //destination.y = -((short int)pos.getY())-heigth/2;
    //cout<<"move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
    return 1;
  }
}

Powerup::Powerup(const char* initfile, double px, double py, double mx, double my, int typ):
  SpaceShip(initfile, px, py, mx, my, -1.0, 0.0){
  energy = 0;
  this->typ = typ; 

  if(0 <= typ && typ <= 1){ // "pwrup_cache.cfg"
    r = g = 255;
    b = 0;
  }
  if(2 <= typ && typ <= 3){ // "pwrup_shield.cfg";
    r = b = 0;
    g = 255;
  }
  if(4 <= typ && typ <= 5){ // "pwrup_rockets.cfg";
    r = g = 0;
    b = 255;
  }
  if(6 <= typ && typ <= 7){ // "pwrup_guns.cfg";
    b = g = 0;
    r = 255;
  }
  if(8 == typ){ // "pwrup_ore.cfg";
    r = 255;
    g = 126;
    b = 0;
  }


  //if(0 <= i && i <= 1) = "pwrup_cache.cfg";
  //if(2 <= i && i <= 3) = "pwrup_shield.cfg";
  //if(4 <= i && i <= 5) = "pwrup_rockets.cfg";
  //if(6 <= i && i <= 7) = "pwrup_guns.cfg";


}

Powerup::~Powerup(){
}

int Powerup::makeMove(){
  //SpaceObject::makeMove();
    lifetime++;
  if(0 > energy)
    return 0;
  else {

    pos += move;
    
    /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
    */
    
      
    destination.x = (short int)pos.getX()-width/2;
    
    destination.y = -((short int)pos.getY())-heigth/2;
    return 1;
  }
}


void Powerup::applyTo(SpaceObject *targetSO){

  SpaceShip* target;
  target = (SpaceShip*)targetSO;

  if(0 <= typ && typ <= 1)
    target->score += 100;
  if(2 <= typ && typ <= 3)
    target->energy += 5000;
  if(4 <= typ && typ <= 5)
    target->rockets += 10;
  if(6 <= typ && typ <= 7)
    target->guns += 1;
  b = 0;

  //cout<<"applied"<<endl;
  
  if(target->MaxEnergy <= target->energy)
    target->energy = target->MaxEnergy;
  if(target->MaxRockets <= target->rockets)
    target->rockets = target->MaxRockets;
  if(target->MaxGuns <= target->guns)
    target->guns = target->MaxGuns;
  this->energy = -1;
  
}


StarBase::StarBase(const char* initfile, double px, double py):
  SpaceShip(initfile, px, py, 0.0, 0.0, -1.0, 0.0, 1 ){
  MaxEnergy = 100000;
  energy    = 100000;
  damage    = 0;
  shieldRegen = 100;
  MaxShield = energy;
  shield = MaxShield;
  r = g = b = 64;

  
  ShipModule* gun;  
  int guns = 2;
  int rockets = 50;
  /*
  //tower rechts
  gun = new ShipModule("guntower.cfg",95.0,0.0,1.0,0.0,M_PI*1/4,-M_PI*1/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  //tower links
  gun = new ShipModule("guntower.cfg",-95.0,0.0,-1.0,0.0,-M_PI*3/4,M_PI*3/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  //tower oben
  gun = new ShipModule("guntower.cfg",0.0,95.0,0.0,1.0,M_PI*3/4,M_PI*1/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  //tower unten
  gun = new ShipModule("guntower.cfg",0.0,-95.0,0.0,-1.0,-M_PI*1/4,-M_PI*3/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
// */

  /*
   */
  float x = 100.0;
  float y = 35.0;
  gun = new ShipModule("guntower.cfg",x,y,1.0,0.0,M_PI*1/4,-M_PI*1/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  gun = new ShipModule("guntower.cfg",-x,y,-1.0,0.0,-M_PI*3/4,M_PI*3/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  gun = new ShipModule("guntower.cfg",y,x,0.0,1.0,M_PI*3/4,M_PI*1/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  gun = new ShipModule("guntower.cfg",y,-x,0.0,-1.0,-M_PI*1/4,-M_PI*3/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  
  gun = new ShipModule("guntower.cfg",x,-y,1.0,0.0,M_PI*1/4,-M_PI*1/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  gun = new ShipModule("guntower.cfg",-x,-y,-1.0,0.0,-M_PI*3/4,M_PI*3/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  gun = new ShipModule("guntower.cfg",-y,x,0.0,1.0,M_PI*3/4,M_PI*1/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  gun = new ShipModule("guntower.cfg",-y,-x,0.0,-1.0,-M_PI*1/4,-M_PI*3/4);
  gun->guns = guns;
  gun->rockets = rockets;
  modules.push_back(gun);
  //  */
}

StarBase::~StarBase(){
  Detonation* deto;
  deto = new Detonation("detonation-200.cfg",
		       this->pos.getX(),
		       this->pos.getY(),
		       this->move.getX(),
		       this->move.getY(), 8);
  GlobalListOfSpaceObjects.push_back(deto);
  Asteorid* asteo;
  int anzahl = 3;
  myVector newpos;
  myVector newspeed;
  newpos.setLength((5)*3);
  newspeed.setLength(0.5);
  for(int i=0; i<anzahl;i++){
    newpos.turn(2*M_PI/anzahl);
    newspeed.turn(2*M_PI/anzahl);
    asteo = new Asteorid("asteroid6.cfg",
			 this->getX()+newpos.getX(),
			 this->getY()+newpos.getY(),
			 this->move.getX()+newspeed.getX(),
			 this->move.getY()+newspeed.getY(),
			 -1.0,0.0, 6);
    GlobalListOfSpaceObjects.push_back(asteo);
  }
}


ShipModule::ShipModule(const char* initfile, double px, double py, double lx, double ly, double maxAng, double minAng):
  SpaceShip(initfile, px, py, 0.0, 0.0, lx, ly, 10 ){
    maximumAngle = maxAng;
    minimumAngle = minAng;
}

ShipModule::~ShipModule(){
  Detonation* deto;
  deto = new Detonation("detonation.cfg",
		       this->pos.getX(),
		       this->pos.getY(),
		       this->move.getX(),
		       this->move.getY(), 8);
  GlobalListOfSpaceObjects.push_back(deto);
}

void ShipModule::aim(SpaceObject &target){
  myVector aimline;
  myVector direction;
  //double distance;
  //direction = (target.pos) - (this->pos);
  aimline = (target.pos + target.move * 10) - (this->pos  + this->move * 10);
  direction = target.pos - this->pos; 
  double deltaAngle = aimline.getAngle() - look.getAngle();
  //cout<<deltaAngle<<endl;
  if(((maximumAngle > minimumAngle) && (maximumAngle > direction.getAngle()) && (minimumAngle < direction.getAngle()))
     || ((maximumAngle < minimumAngle) && ((maximumAngle > direction.getAngle()) || (minimumAngle < direction.getAngle())))){
    if(M_PI/32 > abs(deltaAngle)){
      if(M_PI < abs(deltaAngle))
	if(0 < deltaAngle)
	  this->spin = -abs(deltaAngle)*M_PI;
	else
	  this->spin = abs(deltaAngle)*M_PI;
      else
	if(0 < deltaAngle)
	  this->spin = abs(deltaAngle)*M_PI;
	else
	  this->spin = -abs(deltaAngle)*M_PI;
      
    } else {
      if(M_PI*2 - M_PI/32 < abs(deltaAngle)){
	if(M_PI < abs(deltaAngle))
	  if(0 < deltaAngle)
	    this->spin = -(M_PI*2-abs(deltaAngle))*M_PI;
	  else
	    this->spin = (M_PI*2-abs(deltaAngle))*M_PI;
	else
	  if(0 < deltaAngle)
	    this->spin = (M_PI*2-abs(deltaAngle))*M_PI;
	  else
	    this->spin = -(M_PI*2-abs(deltaAngle))*M_PI;
	
      } else if(M_PI < abs(deltaAngle))
	if(0 < deltaAngle)
	  this->spin = -1.0;
	else
	  this->spin = 1.0;
      else
	if(0 < deltaAngle)
	  this->spin = 1.0;
	else
	  this->spin = -1.0;
    }
  } else
    this->spin = 0.0;
  //cout<<this->spin<<endl;
}



int ShipModule::makeMove(){
  lifetime++;
  if(0 >= energy){
    return 0;
    //energy = MaxEnergy;
    //return 1;
  
  } else {
    double asteoDistance = 200.0;
    double enemyDistance = 600.0;
    bool hasEnemy = false;
    list<SpaceObject*>::iterator it;
    SpaceObject* newTarget = NULL;
    it =  GlobalListOfSpaceObjects.begin();
    while(it !=  GlobalListOfSpaceObjects.end()){
      if((*it)->isTarget()){
	if((*it)->isEnemy() && (enemyDistance > ((*it)->pos - this->pos).getLength())){
	  newTarget = (*it);
	  enemyDistance = ((*it)->pos - this->pos).getLength();
	  hasEnemy = true;
	}
	if(!hasEnemy && (*it)->isAsteorid() && (asteoDistance > ((*it)->pos - this->pos).getLength())){
	  newTarget = (*it);
	  asteoDistance = ((*it)->pos - this->pos).getLength();
	}
      }
      it++;
    }
    setTarget(newTarget);
    
    myVector direction; 
    double deltaAngle;
    if(target){
      this->aim(*target);
      //      this->accelerate(1.0);
      direction = target->getPos() - this->getPos();
      deltaAngle = direction.getAngle() - this->getLook().getAngle();
      if(abs(deltaAngle) < M_PI/(12)){
	this->fire(this);
      }    
      if(nextrock <= lifetime && target->isAsteorid()){
	this->fireRocket(this,target);
	nextrock += 40;
      }
      
    } else {
      this->spin = 0.0;
    }
  }
  
  Uint32 now;
  now = SDL_GetTicks();
  //if(turbotime + 50 <= now){
  this->accelerate(this->accel);
  //}
  this->turn(this->spin);
  if(gunactive) {this->fire(this);}
  if(launcheractive) {this->fireRocket(this,target);}
  pos += move;
  
  //if(NULL == this->target){
  //  selectNearestTarget();
  //}
  
  energy += 50;
  if(MaxEnergy <= energy){
    energy = MaxEnergy;
  }
  
  /*
    if(0 + width/2 > pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(0 + width/2);}
    if(0 + heigth/2 > -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(0 + heigth/2));}
    if(gamemapdim.w - width/2 < pos.getX()) {move.setXY(-move.getX()/2,move.getY()/2); pos.setX(gamemapdim.w - width/2);}
    if(gamemapdim.h - heigth/2 < -pos.getY()) {move.setXY(move.getX()/2,-move.getY()/2); pos.setY(-(gamemapdim.h - heigth/2));}
  */
  
  
  destination.x = (short int)pos.getX()-width/2;
  
  destination.y = -((short int)pos.getY())-heigth/2;
  //cout<<"move(x,y) = "<<move.getX()<<","<<move.getX()<<endl;
  return 1;
}


void ShipModule::fireRocket(SpaceObject *par, SpaceObject *target){
  if(0 < rockets && nextrock <= lifetime){
    rockets--;
    nextrock = lifetime + 20;
    myVector newdir, newpos, mod = look;
    //double spread = M_PI;
    //int guns = 2;
    //guns--;
    newdir.setXY(look.getX(),look.getY());
    //newdir.turn(-M_PI/48);
    //for(int i=0; i<240*2;i++){
    Torpedo *rocket;
    if (0 == nextslot) {
      //mod.turn(-spread/2.0);
      nextslot = 1;
    } else {
      //mod.turn(spread/2.0);
      nextslot = 0;
    }
    //for(int j=0; j<=guns; j++){
      newpos.setXY(getX()+mod.getX()*(width)+look.getX(),
		   getY()+mod.getY()*(heigth)+look.getY());
      rocket = new Torpedo(target,
				  newpos.getX(),
				  newpos.getY(),
				  newdir.getX()*5+move.getX(),
				  newdir.getY()*5+move.getY(),
				  newdir.getX(),
				  newdir.getY());
      rocket->parent = par;
      GlobalListOfSpaceObjects.push_back(rocket);
      //mod.turn(spread/guns);
      //}
      //      cout<<endl;
      //      cout<<"Rocket Fired ("<<(int)rocket % 1000<<")"<<endl;
      //      cout<<"Target is: "<<(int)target % 1000<<endl;
  }
  //newdir.turn(M_PI/240);
  //look.turn(M_PI/240);
  //}
}
