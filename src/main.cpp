#include "openspace.h"
#include "messages.h"
#include "menu.h"
#include "graphics.h"
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_rotozoom.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include "player.h"



int main(int argc, char **argv)
{
  // inizialisierung des Zufalls
  srandom(time(NULL));

  // Hauptanzeigefläche
  SDL_Surface *screen;

  // Initialisierung von SDL:
  if(-1 == SDL_Init(SDL_INIT_VIDEO)){
    cout<<"Fehler beim Initialisieren von SDL: "<<SDL_GetError()<<endl;
    exit(1);
  }

  // komandozeilenargumente parsen (parse.cpp)
  parse(argc,argv);
  
  // Initialisierung des Videomodes:
  if(fs) screen = SDL_SetVideoMode(DISPLAY_W,DISPLAY_H,16, SDL_FULLSCREEN | SDL_DOUBLEBUF);
  else screen = SDL_SetVideoMode(DISPLAY_W,DISPLAY_H,16, SDL_HWSURFACE | SDL_DOUBLEBUF);
  if(NULL == screen)
    {
      cout<<"Fehler bei setzen des Videomoduses: "<<SDL_GetError()<<endl;
      exit(1);
    }

  SDL_Surface *icon;
  SDL_WM_SetCaption("Open Space Shooter - Prototype", "./icon.bmp");
  icon = IMG_Load("data/graphics/icon.bmp");
  SDL_WM_SetIcon(icon, NULL);  
  SDL_ShowCursor(SDL_DISABLE);


  Menu* mainmenu = new Menu(screen);
  
  //  bool done = false;
  bool running = false;
  int level = lv;
  OpenSpace *engine;
  Player *player;

  mainmenu->setTitle("OpenSpaceShooter - Hauptmenu");
  //mainmenu->setOption(1,"(1) Neues Spiel");
  mainmenu->setOption(3,"(3) Vollbildmodus");
  mainmenu->setOption(0,"(0) Programm beenden");

  
  /*
  while(!done){
    if(running){
      mainmenu->setOption(1,"(1) neues Spiel");
      mainmenu->setOption(2,"(2) Spiel fortsetzen");
      
    }
    else {
      mainmenu->setOption(1,"(1) neues Spiel");
      mainmenu->setOption(2,"");

    }    

    switch (mainmenu->run()){
    case 0:
      done = true;
      if(NULL != player && running){
	cout<<setfill('-')<<setw(7)<<player->score<<" Punkte und Level "<<player->level<<" erreicht"<<endl;
      }
      break;
    case 2:
      if(running){
	running = engine->step();      
      }
      break;
    case 3:
      SDL_WM_ToggleFullScreen(screen);
      break;
    case 1:
  */
      player = new Player();
      engine = new OpenSpace(screen, level, player);
      engine->init();
      running = engine->step();      
      if(!(running)){
	//	cout<<setfill('-')<<setw(7)<<player->score<<" Punkte und Level "<<player->level<<" erreicht"<<endl;
      }
/*
      break;
      //default:
      //break;
    }
  }
*/
	cout
	  <<setfill('.')<<setw(7)<<player->score
	  <<" points and "
	  <<"level "
	  <<setfill('.')<<setw(2)<<player->level
	  <<" reached"
	  <<endl;
  SDL_Quit();
  return 0;
}
