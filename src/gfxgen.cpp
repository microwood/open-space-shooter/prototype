#include "openspace.h"
#include <SDL_ttf.h>
#include <SDL_rotozoom.h>
#include <iostream>
#include <string>
#include <sstream>

int main(int argc, char **argv)
{
  if(1 >= argc){
    std::cout<<"not enough parameters"<<std::endl;
    exit(-1);
  }

  // Initialisierung von SDL:
  if(-1 == SDL_Init(SDL_INIT_VIDEO)){
    cout<<"Fehler beim Initialisieren von SDL: "<<SDL_GetError()<<endl;
    exit(1);
  }


  SDL_Surface* pic = SDL_LoadBMP(argv[1]);
  

  /* Create a 32-bit surface with the bytes of each pixel in R,G,B,A order,
     as expected by OpenGL for textures */
  SDL_Surface *surface;
  Uint32 rmask, gmask, bmask, amask;

  /* SDL interprets each pixel as a 32-bit number, so our masks must depend
     on the endianness (byte order) of the machine */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
  rmask = 0xff000000;
  gmask = 0x00ff0000;
  bmask = 0x0000ff00;
  amask = 0x000000ff;
#else
  rmask = 0x000000ff;
  gmask = 0x0000ff00;
  bmask = 0x00ff0000;
  amask = 0xff000000;
#endif

  surface = SDL_CreateRGBSurface(SDL_HWSURFACE, pic->w*16, pic->h, 32,
				 rmask, gmask, bmask, amask);

  if(surface == NULL) {
    fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
    exit(1);
  }
  SDL_SetAlpha(surface,SDL_SRCCOLORKEY,128);


  SDL_Rect source, destination; 
  
  destination.w = pic->w;
  destination.h = pic->h;
  destination.x = 0;
  destination.y = 0;
  source.w = pic->w;
  source.h = pic->h;
  source.x = 0;
  source.y = 0;

  SDL_Surface* tmp = SDL_LoadBMP(argv[1]);

  SDL_BlitSurface(tmp, &source, surface, &destination);
  for(int i=1;i<16;i++){
    tmp = rotozoomSurface(pic, -22.5*i, 1, SMOOTHING_ON);
    destination.x = (pic->w) * (i);
    source.x = (tmp->h-pic->h)/2+1;
    source.y = (tmp->w-pic->w)/2+1;
    SDL_BlitSurface(tmp, &source, surface, &destination);
    }
  SDL_SaveBMP(surface, "a.png");

  return 0;
}
