#ifndef Vector_H
#define Vector_H

#include <SDL.h>

class myVector{
  
  /**
   *  pox und posy sind die komponenten des Vektors;
   */
  double posx, posy;
  /**
   * length ist die Laenge des Vektors und immer Positiv 
   * angle ist der Winkel des Vektors und liegt auf dem Intervall: [PI..-PI)
   */
  double length, angle;
  /**
   * Da sich fuer manche Operationen die Polardarstellung und fuer andere die
   * Darstellung in der Koordinatenform besser eignet, wurde ein Flag gesetzt,
   * Was kennzeichnet, welche Variablen gerade die gueltiegen Werte haben.
   * polar = true => length & angle
   * polar = false => posx & posy
   * Bei bedarf wird dann in die Jeweils in die eine oder andere umgewandelt.
   */
  bool polar;

  void enablePolar();
  void disablePolar();
  void fitAngle();


 public:  
  myVector();
  ~myVector();


  void toScreen();
  //char* toString();
  void show(SDL_Surface *screen);

  void setLength(double newlen);
  void setAngle(double newang);
  void setX(double newx);
  void setY(double newy);
  void setXY(double newx, double newy);
  void setValue(myVector newv);


  void turn(double ang);
  // proj() Projeziert den Vektor auf die Gerade (dest)
  myVector proj(myVector dest);
  //myVector minus(const myVector sub);


  double getLength();
  double getAngle();
  double getX();
  double getY();

  friend myVector operator+(myVector vector, myVector vector);
  friend myVector operator-(myVector vector, myVector vector);

  friend myVector operator*(int skalar, myVector vector);
  friend myVector operator*(double skalar, myVector vector);
  friend myVector operator*(myVector vector, int skalar);
  friend myVector operator*(myVector vector, double skalar);
  //friend myVector operator*(myVector vector, myVector vector);

  myVector& operator+=(const myVector summand);
  myVector& operator-=(const myVector summand);
  myVector& operator*=(const double skalar);


};


#endif
