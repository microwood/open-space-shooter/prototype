#ifndef MENU_H
#define MENU_H

#include <SDL.h>
#include <SDL_ttf.h>



class Menu{
 public:
  Menu(SDL_Surface *nscreen);
  void setTitle(char* text){this->title = text;}
  void setOption(int number, char* text){this->options[number] = text;}
  int run();

  ~Menu();
 private:
  char* title;
  char* options[10];
  TTF_Font *font;
  SDL_Color text_color;
  int size;
  SDL_Rect dest;
  SDL_Surface *screen;
  void printline(char* text);
};

int menu(SDL_Surface *screen);





#endif
