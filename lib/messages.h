#ifndef Messages_H
#define Messages_H

#include <SDL.h>
#include <SDL_ttf.h>

#include <sstream>

//static const int MAX_STACK = 100;

struct TextLine{
  int duration;
  string text;
};

namespace MessageBox
{
  static list<TextLine> ListOfMessages;
     
  static void show(SDL_Surface *screen){
    
    if(!TTF_WasInit() && -1 == TTF_Init()) {
      printf("TTF_Init: %s\n", TTF_GetError());
      exit(1);
    }
    TTF_Font *font;
    char* font_file = "data/Neuropol.ttf";
    int size = 16;
    if( (font = TTF_OpenFont(font_file, size)) == NULL) {
      printf("TTF_OpenFont error: %s", TTF_GetError());
      exit (-1);
    }
    SDL_Color text_color;
    text_color.r = 0xff;
    text_color.g = 0xff;
    text_color.b = 0xff;
    SDL_Rect dest;
    dest.x = 100 + size;
    dest.y = size;
    SDL_Surface *text_surf;

    stringstream scrstream;

   
    list<TextLine>::iterator it;
    it = ListOfMessages.begin();
    while(it != ListOfMessages.end()){
      text_surf = TTF_RenderText_Blended(font, (*it).text.c_str(), text_color);
      SDL_BlitSurface(text_surf, NULL, screen, &dest);      
      SDL_FreeSurface(text_surf);
      dest.y += size;
      if(0 >= (*it).duration--)
	it = ListOfMessages.erase(it);
      else {
	if(it != ListOfMessages.end()){
	  it++;
	} else {
	  break;
	}	
      }
    }
    TTF_CloseFont(font);
  }

  static void addMessage(string txt){
    TextLine newcontent;
    if((ListOfMessages.begin() != ListOfMessages.end())){
      list<TextLine>::iterator it;
      it = ListOfMessages.begin();
      // hier muss nur auf das letze element in der liste zugegriffen werden, end() liefert mir die Position hinter dem letzen und ich kann grade nicht rausfinden wie das sonst gehen sollte...
      while(it != ListOfMessages.end()){
	newcontent.duration = (*it).duration + 100;
	it++;
      }
    }
    else
      newcontent.duration = 150;
    newcontent.text = txt;
    ListOfMessages.push_back(newcontent);
  }

  static void clear(){
    ListOfMessages.clear();
  }
};


//list<string> MessageBox::ListOfMessages;


#endif
