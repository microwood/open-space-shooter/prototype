#ifndef Miner_H
#define Miner_H

#include <SDL.h>
#include <list>
#include <SDL_image.h>
#include <iostream>
#include <sstream>
#include "parse.h"
#include "spaceobjects.h"
#include "spaceships.h"
#include "graphics.h"
#include "rank.h"
#include "player.h"


using namespace std;


extern list<SpaceObject*> GlobalListOfSpaceObjects;
extern list<PlayerShip*> GlobalListOfPlayerShips;
extern list<SpaceObject*> GlobalListOfPassiveObjects;
extern SDL_Rect gamemapdim;
extern bool myPause;



class CollisionsSector{
  list<SpaceObject*> GlobalListOfSpaceObjects;
  int x,y;
 public:
  CollisionsSector();
};

class SpaceSector{
  SDL_Surface *background, *gamemap;
  SpaceObject *target_dir;
  int x,y;
 public:
  SpaceSector();
  //SpaceSector(int x, int y);
  ~SpaceSector();
  SDL_Surface* draw(int x, int y);
};


struct Coordinate{
  int x;
  int y;
};


class OpenSpace{
  gcn::SDLInput* input;           // Input driver
  gcn::SDLGraphics* graphics;     // Graphics driver

  gcn::ImageLoader* imageLoader;  // For loading images
  gcn::Gui* gui;                  // A Gui object - binds it all together
  gcn::Container* top;            // A top container
  gcn::ImageFont* font;           // A font
  gcn::Label* label;              // And a label for the Hello World text


  SDL_Surface *screen, *status, *statusframe, *minimap, *bigmap;
  SDL_Surface *bg1, *bg2, *bg3, *bg4;
  SDL_Rect bgview1, bgview2, bgview3, bgview4;
  SDL_Rect viewport_nw, viewport_n, viewport_no, viewport_w, viewport_m, viewport_o, viewport_sw, viewport_s, viewport_so;
  SDL_Rect mainscreen, statusscreen, target;
  Player* player;
  int level;
  //map<int,map<int,SpaceSector> > sectors;
  SpaceSector sector;
 public:
  OpenSpace(SDL_Surface *screen, int level=1, Player *player = NULL);
  ~OpenSpace();
  void reset(int level);
  
  //Player* run();

  void run();
  // run is init and step until step is true;
  void init();
  bool step();
 private:
  bool quit;
  int mousex, mousey;
  SpaceObject* cursor;
  Uint32 endtime;
  bool done; // Abbruchbedingung fuer die Hauptschleife
  bool hasenemies;
  bool clear;
  bool wantleave;
  bool showmap;
  int zoom;
  int position_x;
  int position_y;
  int objectcount;
  SDL_Rect mapposition;
  SDL_Event event;
  myVector direction; 
  list<SpaceObject*>::iterator it;
  list<PlayerShip*>::iterator itp;
  list<SpaceObject*>::iterator ity;
  map<int,list<SpaceObject*> >::iterator mapit;
  stringstream output;

};


#endif
