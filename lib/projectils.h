#ifndef PROJECTILS_H
#define PROJECTILS_H

#include "spaceobjects.h"

class Bullet: public SpaceObject{
  int expire;
  int i;
 public:
  Bullet(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=-1.0, double ly=0.0, int m=10, int dur=15);
  ~Bullet();
  int makeMove();
  virtual bool isBullet() {return true;}

};

class Rocket: public SpaceObject{
  int expire;
  int i;
 public:
  Rocket( SpaceObject *target, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0, int m=50);
  ~Rocket();
  int makeMove();
  void aim(SpaceObject &target);
  virtual bool isBullet() {return true;}
  virtual bool isEnemy();
};

class Torpedo: public SpaceObject{
  int expire;
  int i;
 public:
  Torpedo( SpaceObject *target, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0, int m=50);
  ~Torpedo();
  int makeMove();
  void aim(SpaceObject &target);
  virtual bool isBullet() {return true;}
  virtual bool isEnemy();
};

#endif
