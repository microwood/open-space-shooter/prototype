#ifndef _PARSE_H
#define _PARSE_H

// globale Variablen, die Geparst werden:
//level und lifes, fullscreen und maus-modus (no mouse)
extern int lv;
extern int li;
extern bool fs;
extern bool nm;

// Funktion, die parst
int parse(int argc, char **argv);

#endif
