#ifndef PLAYER_H
#define PLAYER_H

#include "rank.h"
#include "spaceships.h"


class Player{
 public:
  Player();
  PlayerShip* ship;
  int level;
  int lifes;
  int cache;
  int score;
  Rank rank;
  int lastScoreGotLive;
  int lastScoreGotHigh;
  int nextScoreGetHigh;
};



#endif // PLAYER_H
