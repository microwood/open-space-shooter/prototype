#ifndef RANK_H
#define RANK_H

#include <SDL.h>
#include <SDL_image.h>


// Der Dienstgrad,
class Rank{
  // Bilddatei
  SDL_Surface *image;
  // zum anzeigen der Grafiken
  SDL_Rect destination, source;
  // aktuelle Stufe und Laufbahn
  int stufe, laufbahn;
public:
  Rank();
  void show(SDL_Surface *screen, int xpos, int ypos);
  // Bef�rderung:
  bool incRank();
  // Laufbahn�nderung:
  bool setLaufbahn(int newL);

  void decRank();
  
  int getRank();
  int getLaufbahn();
  void setRank(int newR);
};




#endif //RANK_H
