#ifndef Space_Objects_H
#define Space_Objects_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "vector.h"
#include <list>

#include <map>

using namespace std;


class SpaceObject {
 public:
  int lifetime;
  myVector pos;
  myVector look;
  myVector move;
  static map<string,SDL_Surface*> LoadedImages;
  SDL_Surface *image;
  SDL_Surface *hit;
  SDL_Rect destination, source;
  int width, heigth;
  int mass;
  double accel, spin;
  double MaxAccel;
  //int guns, MaxGuns;
  //int rockets, MaxRockets;
  int MaxSpeed;
  int energy, MaxEnergy;
  int damage;
  float agility;
  int nextaction;
  bool hidden;
  int gotHit;
  SpaceObject* target;
  SpaceObject* parent;
  int score;
  int r,g,b;
  virtual void setTarget(SpaceObject* target);
  void isNomoreTrackedBy(SpaceObject* tracker);
  void isTrackedBy(SpaceObject* tracker);
  list<SpaceObject*> TrackedBy;
  void unsetTarget(SpaceObject* target=NULL);

  SpaceObject(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0);
  virtual ~SpaceObject();

  //getter:
  myVector getPos(){ return this->pos; }
  myVector getLook(){ return this->look; }
  int getWidth(){ return this->width; }
  int getHeigth(){ return this->heigth; }
  int getX(){ return (int)this->pos.getX(); }
  int getY(){ return (int)this->pos.getY(); }

  virtual int outScore() {return this->MaxEnergy/100000;}

  SpaceObject* nextTo(double dist);
    
  //setter:
  void setX(double newx) {pos.setX(newx); destination.x = (short int)pos.getX()-width/2;}
  void setY(double newy) {pos.setY(newy); destination.y = -(short int)pos.getY()-width/2;}
  void setXY(double newx, double newy) { setX(newx); setY(newy);}
  void setPos(myVector newPos) { setXY(newPos.getX(),newPos.getY()); }

  void setAccel(double newacc){accel = newacc;}
  void setSpin(double newspin){spin = newspin;}
  void addSpin(double summand){spin += summand;}
  //  void useGun(bool status)    {gunactive = status;}
  void accelerate(double acc=1.0);
  //void turbo();
  void turn(double ang);
  virtual void aim(SpaceObject &target);
  virtual void aim(myVector direction);
  virtual int makeMove();
  void hide() {hidden = true;}
  void unHide() {hidden = false;}
  virtual void show(SDL_Surface *screen, int x, int y);
  void indicate_direction(int px, int py, SpaceObject &target, SDL_Surface *screen);
  void indicate(SDL_Surface *screen, int rel_x, int rel_y, int range_x, int range_y);

  void showbar(int px, int py, int wi, int he, int value, int maxval, SDL_Surface *screen, int fr=255, int fg=0, int fb=0, int ovr=255, int ovg=64, int ovb=0, int tr=0, int tg=255, int tb=0);
  void showenergy(int px, int py, int wi, int he, SDL_Surface *screen);
  virtual void showshield(int px, int py, int wi, int he, SDL_Surface *screen){}
  void showint(int px, int py, int wi, int he, int value, SDL_Surface *screen);
  void showHit();
  
  //virtual void elasticStoss(SpaceObject *hitmate);
  virtual void collision(SpaceObject *hitmate);
  virtual bool collisionsCheck(SpaceObject *hitmate);
  virtual void applyTo(SpaceObject *target){}
  virtual bool takeDamage(int damage);

  virtual bool isPlayer() {return false;}
  virtual bool isEnemy() {return false;}
  virtual bool isEnemyShip() {return false;}
  virtual bool isSpaceShip() {return false;}
  virtual bool isTransparent() {return false;}
  virtual bool isTarget() {return false;}
  virtual bool isPowerup() {return false;}
  virtual bool isBullet() {return false;}
  virtual bool isAsteorid() {return false;}
  virtual bool isMeta() {return false;}

};

class MetaSpaceObject: public SpaceObject{

 public:
  MetaSpaceObject(const char* initfile="blank.cfg", double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0);
  virtual ~MetaSpaceObject();

  //  addSpaceObject(const char* initfile, double px=0.0, double py=0.0, double lx=0.0, double ly=0.0);

  void addObject(SpaceObject *newObject);

  virtual int makeMove();
  virtual void show(SDL_Surface *screen, int x, int y);
  virtual void elasticStoss(SpaceObject *hitmate);
  virtual void collision(SpaceObject *hitmate);
  virtual bool collisionsCheck(SpaceObject *hitmate);
  virtual void applyTo(SpaceObject *target);
  virtual bool takeDamage(int damage);

  bool Player;
  bool Enemy;
  bool EnemyShip;
  bool SpaceShip;
  bool Transparent;
  bool Target;
  bool Powerup;
  bool Bullet;
  bool Asteorid;

  virtual bool isPlayer() {return Player;}
  virtual bool isEnemy() {return Enemy;}
  virtual bool isEnemyShip() {return EnemyShip;}
  virtual bool isSpaceShip() {return SpaceShip;}
  virtual bool isTransparent() {return Transparent;}
  virtual bool isTarget() {return Target;}
  virtual bool isPowerup() {return Powerup;}
  virtual bool isBullet() {return Bullet;}
  virtual bool isAsteorid() {return Asteorid;}
  virtual bool isMeta() {return true;}


  list<SpaceObject*> ListOfSpaceObjects;


};



class Asteorid: public SpaceObject{
  int instanz;
 public:
  Asteorid(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0, int inst=1 );
  ~Asteorid();
  //bool isEnemy() { if(2 >= instanz) return true; else return false;}
  //bool isEnemyShip() { if(2 >= instanz) return true; else return false;}
  bool isTarget() { if(2 >= instanz) return false; else return true;}
  virtual bool isAsteorid() {return true;}
};

class Detonation: public SpaceObject{
  int expire;
  int i;
 public:
  Detonation(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, int dur=15);
  ~Detonation();
  int makeMove();
  virtual bool isTransparent() {return true;}
};



#endif
