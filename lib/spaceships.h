#ifndef SPACESHIPS_H
#define SPACESHIPS_H

#include <math.h>
#include "spaceobjects.h"
#include <list>

class SpaceShip: public SpaceObject{
 public:

  int guns, MaxGuns;
  int rockets, MaxRockets;
  int nextslot;
  int nextrock;
  int nextshot;
  bool gunactive;
  bool launcheractive;
  int shield, MaxShield;
  int shieldRegen;

  list<SpaceShip*> modules;

  SpaceShip(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0, int m=10 );
  ~SpaceShip();
  void useGun(bool status);
  void useLauncher(bool status);
  virtual void fire(SpaceObject *parent);
  virtual void fireRocket(SpaceObject *parent, SpaceObject *target);

  void setGuns(int ng){ if(ng>MaxGuns){guns = MaxGuns;}else{guns = ng;}}
  void setRockets(int nr){ if(nr>MaxRockets){rockets = MaxRockets;}else{rockets = nr;}}


  
  virtual void show(SDL_Surface *screen, int x, int y);
  
  virtual bool takeDamage(int damage);
  virtual void showshield(int px, int py, int wi, int he, SDL_Surface *screen);
  virtual int makeMove();

  virtual int outScore() {return (int)this->MaxEnergy/1000 + this->MaxShield/100 * (this->guns + 1);}

  virtual bool isSpaceShip() {return true;}

};

class PlayerShip: public SpaceShip{
  bool targetlock;
  bool ki;

 public:
  SpaceObject* targetIndicator;
  SpaceObject* target_dir;
  PlayerShip(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0, int m=10, int ki=false);
  ~PlayerShip();
  void show(SDL_Surface *screen, int x, int y);
  void aim(SpaceObject &target);
  int makeMove();
  void selectNextTarget();
  void selectNextEnemyTarget();
  void selectNearestTarget();
  void targetLock();
  void targetUnlock();
  bool targetIsLocked();
  void setTarget(SpaceObject* target);
  void showguns(int px, int py, int wi, int he, SDL_Surface *screen);
  void showrockets(int px, int py, int wi, int he, SDL_Surface *screen);

  virtual bool isPlayer() {return true;}

};


class EnemyShip: public SpaceShip{
  int type;
 public:
  EnemyShip(const char* initfile, int t=1, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, double lx=0.0, double ly=0.0, int m=10 );
  ~EnemyShip();
  void antiaim(SpaceObject &target);
  int makeMove();
  bool isEnemy() { return true;}
  bool isEnemyShip() { return true;}
  bool isTarget() { return true;}
};

class Powerup: public SpaceShip{
 public:
  int typ;
  Powerup(const char* initfile, double px=0.0, double py=0.0, double mx=0.0, double my=0.0, int typ=0);
  ~Powerup();
  int makeMove();
  void applyTo(SpaceObject *target);
  virtual bool isPowerup() {return true;}
  virtual bool isSpaceShip() {return false;}

};



class StarBase: public SpaceShip{
 public:
  StarBase(const char* initfile, double px=0.0, double py=0.0);
  ~StarBase();
  virtual int outScore() {return -(int)this->MaxEnergy/10;}

  virtual bool isTarget() {return true;}
  virtual bool isPlayer() {return true;}
};

class ShipModule: public SpaceShip{
  double maximumAngle;
  double minimumAngle;
 public:
  ShipModule(const char* initfile, double px=0.0, double py=0.0, double lx=0.0, double ly=0.0, double maxAng=M_PI, double minAng=-M_PI);
  ~ShipModule();
  virtual bool isTarget() {return true;}
  int makeMove();
  void aim(SpaceObject &target);
  void fireRocket(SpaceObject *parent, SpaceObject *target);
  
};



#endif
